#include "../pch.h"
#include "by_jeffset_embedded_test_MyRIO.h"
#include "../device.h"

struct nativeData {
    deithvenbot::MyRioFpga myRioFpga;
    deithvenbot::MyRioLEDBoard ledBoard;
    nativeData() : myRioFpga(), ledBoard() {}
} * pNativeData;

jboolean Java_by_jeffset_embedded_test_MyRIO_led__I(JNIEnv*, jobject obj, jint led) {
    return (jboolean) pNativeData->ledBoard.led(led);
}
void Java_by_jeffset_embedded_test_MyRIO_init(JNIEnv*, jobject obj) {
    pNativeData = new nativeData();
}
jboolean Java_by_jeffset_embedded_test_MyRIO_toggle(JNIEnv*, jobject obj, jint led) {
    return (jboolean) pNativeData->ledBoard.toggle(led);
}
void Java_by_jeffset_embedded_test_MyRIO_switchAllOff(JNIEnv*, jobject obj) {
    pNativeData->ledBoard.switchOffAll();
}
jint Java_by_jeffset_embedded_test_MyRIO_getLedCount(JNIEnv*, jobject) {
    return 4;
}
jboolean Java_by_jeffset_embedded_test_MyRIO_led__IZ(JNIEnv*, jobject obj, jint led, jboolean enable) {
    return (jboolean) pNativeData->ledBoard.led(led, enable);
}
void Java_by_jeffset_embedded_test_MyRIO_release(JNIEnv*, jobject obj) {
    delete pNativeData;
}
