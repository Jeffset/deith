//
// Created by marco on 09/11/16.
//

#ifndef PTHREAD_TEST_TASK_H
#define PTHREAD_TEST_TASK_H

#define UNUSED_PARAM(x)

#include "pthread.h"
#include <tuple>
#include <functional>

template<class Tuple, class Fct, bool Enough, int Total, int N, int... Ns>
struct _unpack_impl {
    using _Fct = std::decay_t<Fct>;
    inline static auto generate(Tuple tuple, Fct fct) {
        return _unpack_impl<Tuple, Fct, Total - 1 <= N, Total, N + 1, Ns..., N>::generate(
                std::forward<decltype(tuple)>(tuple),
                std::forward<decltype(fct)>(fct));
    }
};
template<class Tuple, class Fct, int Total, int N, int... Ns>
struct _unpack_impl<Tuple, Fct, true, Total, N, Ns...> {
    inline static auto generate(Tuple tuple, Fct fct) {
        return fct(std::get<Ns>(tuple)...);
    }
};
template<class Tuple, class Fct>
auto unpack_tuple(typename std::enable_if<(std::tuple_size<Tuple>::value > 0),const Tuple&>::type t,const Fct& fct) {
    using _Tuple = std::decay_t<Tuple>;
    return _unpack_impl<decltype(t), decltype(fct), false, std::tuple_size<_Tuple>::value, 0>::generate(
            std::forward<decltype(t)>(t),
            std::forward<decltype(fct)>(fct));
}

template<class Tuple, class Fct>
auto unpack_tuple(typename std::enable_if<(std::tuple_size<Tuple>::value == 0),const Tuple&>::type t,const Fct& fct) {
    UNUSED_PARAM(t)
    return fct();
}


template<class Fct, class Ret, class...Args>
struct _task_impl {
    static inline void run(pthread_t* _thread, Fct fct, Args ...args) {
        struct _args {
            std::tuple<Args...> tuple_of_args;
            Fct fct;
            _args(std::tuple<Args...>&& params, Fct&& f)
                    : tuple_of_args(std::forward<decltype(params)>(params)),
                      fct(std::forward<decltype(f)>(f)) {}
        } * args_pack = new _args{
                std::make_tuple(std::forward<Args>(args)...),
                std::forward<decltype(fct)>(fct)
        };
        auto wrapper = [](void* packed_params) -> void* {
            _args* params = (_args*) packed_params;
            Ret ret = unpack_tuple<decltype(params->tuple_of_args), decltype(params->fct)>
                    (params->tuple_of_args, params->fct);
            delete params;
            return new Ret(std::move(ret));
        };
        pthread_create(_thread, nullptr, wrapper, args_pack);
    }
};

template<class Fct, class...Args>
struct _task_impl<Fct, void, Args...> {
    static inline void run(pthread_t* _thread, Fct&& fct, Args&& ...args) {
        struct _args {
            std::tuple<Args...> tuple_of_args;
            Fct fct;
            _args(std::tuple<Args...>&& params, Fct&& f)
                    : tuple_of_args(std::forward<decltype(params)>(params)),
                      fct(std::forward<decltype(f)>(f)) {}
        } * args_pack = new _args{
                std::make_tuple(std::forward<Args>(args)...),
                std::forward<decltype(fct)>(fct)
        };
        auto wrapper = [](void* packed_params) -> void* {
            _args* params = (_args*) packed_params;
            unpack_tuple<decltype(params->tuple_of_args), decltype(params->fct)>
                    (params->tuple_of_args, params->fct);
            delete params;
            return nullptr;
        };
        pthread_create(_thread, nullptr, wrapper, args_pack);
    }
};
template<class Ret>
struct _task_impl_ret {
    static inline Ret $return(pthread_t* _thread) {
        Ret* ret_val;
        pthread_join(*_thread, (void**) &ret_val);
        return *ret_val;
    }
};

template<>
struct _task_impl_ret<void> {
    static inline void* $return(pthread_t* _thread) {
        void* ret_val;
        pthread_join(*_thread, &ret_val);
        return ret_val;
    }
};

class task {
    pthread_t _thread;
public:
    enum state {
        unstarted, running, detached, joined
    } _state;
    template<class Fct, class...Args>
    explicit task(Fct&& fct, Args... args) {
        _task_impl<Fct, decltype(fct(args...)), Args...>::run(&_thread, std::forward<decltype(fct)>(fct),
                                                              std::forward<decltype(args)>(args)...);
        _state = running;
    }
    task() : _thread(0), _state(unstarted) {}
    template<class Ret = void>
    auto join() {
        if (_thread == 0)
            throw std::runtime_error("can't join not running task!");
        auto val = _task_impl_ret<Ret>::$return(&_thread);
        _thread = 0;
        _state = joined;
        return val;
    }
    task(const task&) = delete;
    void operator=(const task&) = delete;
    task(task&& r_task) {
        _thread = r_task._thread;
        _state = r_task._state;
        r_task._thread = 0;
    }
    task& operator=(task&& r_task) {
        if (_state == running)
            throw std::runtime_error("can't move into running task!");
        _thread = r_task._thread;
        _state = r_task._state;
        r_task._thread = 0;
        return *this;
    }
    state state() const {
        return _state;
    }
    void detach() {
        if (_state != running)
            throw std::runtime_error("can't detach not running task!");
        pthread_detach(_thread);
        _state = detached;
        _thread = 0;
    }
    ~task() {
        if (_thread != 0) {
            std::terminate();
        }
    }
};

#endif
