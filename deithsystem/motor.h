//======================= motor.h ============================
/** @brief
 * Классы для работы с DC-моторами
 * - MyRioServoDriver
 * - MyRioServo <- MyRioPreciseServo, MyRioContinuousServo
***************************************************************/
#ifndef _BEDPO_MOTOR_
#define _BEDPO_MOTOR_

#include "device.h"
#include "motor_interface.h"

//! \brief Пространство имен библиотеки классов для работы с BEDPO
namespace deithvenbot {

//------------------------------------------------------------------------
//====================== MOTOR SYSTEM ====================================
//------------------------------------------------------------------------

//! \brief Низкоуровневое обозначение мотора для драйвера моторов (Прямо не используется)
enum class Motor {
    M1, M2
};

//! \brief Поддерживаемые режимы работы драйвера моторов
enum class MotorMode {
    //! \brief Подать определенную мощность на мотор и не контролировать ее.
            PowerCtrl,
    
    //! \brief Задать "скорость" вращения мотора (драйвер будет использовать ПИД-регулятор мощности для поддержания скорости)
            ConstSpeed,
    
    //! \brief Езда с постоянной скоростью по значениям энкодера
            ToPosition,
    
    //! Служебный псевдорежим который сбрасывает значения энкодеров и мощности в ноль, а режим - в PowerCtrl
            ResetEncoder
};

/**
 * @brief Класс, представляющий физическое устройство: драйвер моторов (DC Motor Driver for TETRIX).
 * Производит настройку драйвера моторов через протокол i2c.
 * Необходим для работы программных (логичеких) устройств (моторов)
 * либо непосредственной работы с устройством драйвера
 */
class DCMotorsDriver : public StandardI2CDevice {
private:
    byte _GetMode(Motor motor);
    void _SetMode(Motor motor, byte mode);
public:
    //! \brief По истечении 2,5 секунд драйвер сбрасывает мощность мотора
    //! \param nto истина - запретить таймаут выключения, ложь - разрешить
    void SetNoTimeout(bool nto);
    
    //! \param motor М1 или М2
    //! \return значения бита внутренней ошибки драйвера моторов
    bool IsError(Motor motor);
    
    //! \brief Используется в режиме езды по энкодерам
    //! \param motor М1 или М2
    //! \return возвращает истину если мотор "все еще едет" (если текущее значение энкодера не совпадает с целевым)
    bool IsBusy(Motor motor);
    
    //! \brief Задает режим реверса для мотора (Все действия мотора будут в противоположную сторону)
    void SetReverse(bool rev, Motor motor);
    bool GetReverse(Motor motor);
    
    //! \brief Задает низкоуровневый режим работы моторов
    //! \param mode режим работы мотора либо псевдорежим сброса энкодера
    //! \param motor
    void SetCurrentMode(MotorMode mode, Motor motor);
    MotorMode GetCurrentMode(Motor motor);
    
    /**
     * @brief
     * @param power Значение мощности/скорости [-100,+100],-128
     * В неконтролируемом режиме задает мощность мотора, 0 - Стоп/Тормоз, -128 - Стоп/Отпустить мотор
     * В контролируемом доступе, значения [-100,+100] отображаются в [-1000,+1000] оборотов в секунду.
     * @param motor M1 или M2
     */
    void SetPower(sbyte power, Motor motor);
    sbyte GetPower(Motor motor);
    
    //! \brief Возвращает напряжение на аккумуляторе
    //! \return Напряжение в вольтах
    float GetBatteryVoltage();
    
    //! \brief Возвращает текущее значение энкодера на моторе
    //! \return
    long GetCurrentEncoder(Motor motor);
    /**
     * @brief Переводит выбранный мотор в псевдорежим ResetEncoder,
     * что сбрасывает значения энкодеров, мощности и устанавливает режим в PowerCtrl
     * @param motor M1 или M2
     */
    void Reset(Motor motor);
    
    /**
     * @brief Задает целевое значение энкодера
     * @param value Значение (любое значение 32-разрядное знаковое целое), 1440 единиц на один оборот
     * @param motor M1 или M2
     */
    void SetTargetEncoder(long value, Motor motor);
    long GetTargetEncoder(Motor motor);
public:
    //! \brief Создает объект физического устройства (никаких передач данных не производится)
    //! \param deviceChainNum адрес (not shifted) устройства в i2c цепи
    //! \param slot Физический порт контроллера myRio (А либо B)
    DCMotorsDriver(byte deviceChainNum, II2CSlot* slot);
    ~DCMotorsDriver();
};

/**
 * @brief класс, описывающий логическое устройство одного мотора, контролируемого драйвером
 */
class DCMotor : public IPreciseServoMotor {
public:
    static const long UNITS_PER_360ROLL = 1440;
    static const sbyte NEUTRAL_POWER = -128;
private:
    DCMotorsDriver* _driver;
    Motor _motor;
    long _rem_encoder;
public:
    //! \brief Создает объект логического устройства одного мотора
    //! \param driver ссылка на физический драйвер моторов
    //! \param motor номер (М1 или М2) мотора в нотации драйвера, с которым ассоциировать данный объект
    //! \param reverse задавать ли реверс выбранному мотору (например, для настройки левого/правого мотора)
    DCMotor(DCMotorsDriver* driver, Motor motor, bool reverse);
    ~DCMotor();
    
    //! \return Есть ли мощность на моторах
    bool IsMotorOn();
    
    //! \brief блокировать выполнение до завершения езды (для режима езды по энкодерам)
    void WaitForMotor();
    
    //! \brief Перевести мотор режим контролируемой скорости
    //! \param speed скорость (см документацию на драйвер)
    void RideConstSpeed(sbyte speed);
    //! \brief Перевести мотор в неконтролируемый режим
    //! \param power значение мощности
    void RideJustPower(sbyte power);
    //! \brief Перевести мотор в режим езды по энкодерам
    //! \param rolls кол-во оборотов (можно использовать функции для перевода из см или мм)
    //! \param speed скорость
    //! \param wait блокировать ли до достижения нужного положения
    void RidePreciseNRolls(float rolls, sbyte speed, bool wait = true);
    
    //! \brief Задать нулевую мощность = затормозить
    void StopBrake();
    //! \brief Задать нейтральную мощность = снять питание с моторов
    void Stop();
    
    void RememberPosition();
    float GetDeltaPosition();
    // --------- COMMON PRECISE SERVO INTERFACE --------------
    //! \brief Задает нужный режим, чтобы мотор мог работать в режиме сервомотора (+сброс,timing=6)
    void ServoMode();
    float GetPosition() const override;
    void SetPosition(float pos) override;
    void AdvancePosition(float offset) override;
    void SetTiming(byte timing) override;
};

class DCMotorDifChassis {
    DCMotor* _leftMotor;
    DCMotor* _rightMotor;
public:
    enum Wheels {
        Left, Right
    };
public:
    DCMotorDifChassis(DCMotor* _left, DCMotor* _right)
            : _leftMotor(_left), _rightMotor(_right) {
    }
    void WaitForMotors();
    //
    void RideConstSpeed(sbyte speed);
    void RideJustPower(sbyte power);
    void RideRolls(float rolls, sbyte speed, bool wait);
    
    void StopBrake();
    void Stop();
    
    void TurnCentered(float angle, sbyte speed, bool wait);
    void TurnOnWheel(float angle, Wheels center, sbyte speed, bool wait);
    
    void RememberPositon();
    float GetDeltaPosition();
};

constexpr float operator "" _cm(long double cms) {
    return float(cms) / 32.0f;
}
}
#endif