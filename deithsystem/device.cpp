#include "device.h"

extern NiFpga_Session myrio_session;

namespace deithvenbot {
using namespace std;

MyRioFpga::MyRioFpga() {
    logi("MyRIO FPGA: Init...");
    NiFpga_Status status = MyRio_Open();
    if (status)
        throw std::runtime_error("Failed to initialize MyRIO FPGA!");
}
MyRioFpga::~MyRioFpga() {
    logi("MyRIO FPGA: Destruct...");
    MyRio_Close();
    logi("MyRIO FPGA: Destructed.");
}

enum DriverAddr {
    Version = 0x0,
    Manufacturer = 0x8,
    SensorType = 0x10,
};

SynchronizedDevice::SynchronizedDevice() {
    pthread_mutex_init(&_mutex, nullptr);
}
SynchronizedDevice::~SynchronizedDevice() {
    pthread_mutex_destroy(&_mutex);
    
}

I2CPort::I2CPort(MyRioPort letter) {
    uint8_t config;
    switch (letter) {
        case MyRioPort::A:
            I2CxADDR = I2CAADDR;
            I2CxDATO = I2CADATO;
            I2CxDATI = I2CADATI;
            I2CxSTAT = I2CASTAT;
            I2CxCNTL = I2CACNTL;
            I2CxGO = I2CAGO;
            //
            READ_U8(SYSSELECTA, &config);
            WRITE_U8(SYSSELECTA, config | (1 << 7)); // �������� ������ i2c
            WRITE_U8(I2CACNFG, 0x1); // �������� i2c
            WRITE_U8(I2CACNTR, 213); // �������: 213 (=100 kb/ps)
            break;
        case MyRioPort::B:
            I2CxADDR = I2CBADDR;
            I2CxDATO = I2CBDATO;
            I2CxDATI = I2CBDATI;
            I2CxSTAT = I2CBSTAT;
            I2CxCNTL = I2CBCNTL;
            I2CxGO = I2CBGO;
            //
            READ_U8(SYSSELECTB, &config);
            WRITE_U8(SYSSELECTB, config | (1 << 7)); // �������� ������ i2c
            WRITE_U8(I2CBCNFG, 0x1); // �������� i2c
            WRITE_U8(I2CBCNTR, 213); // �������: 213 (=100 kb/ps)
            break;
        default:
            throw invalid_argument("Invalid port specified!");
            break;
    }
}
I2CPort* I2CPort::_A = nullptr;
I2CPort* I2CPort::_B = nullptr;
I2CPort* I2CPort::_C = nullptr;
I2CPort* I2CPort::A() {
    if (_A == nullptr)
        _A = new I2CPort(MyRioPort::A);
    return _A;
}
I2CPort* I2CPort::B() {
    if (_B == nullptr)
        _B = new I2CPort(MyRioPort::B);
    return _B;
}
I2CPort* I2CPort::C() {
    if (_C == nullptr)
        _C = new I2CPort(MyRioPort::C);
    return _C;
}
byte I2CPort::ReadI2CSingleByte(byte _deviceChainNum) {
    locker lock(this);
    byte val;
    uint8_t stat;
    //--------------- ����� ������ -------------------
    WRITE_U8(I2CxADDR, (_deviceChainNum << 1) | 0x1);
    //----------- ������ ������ ����-"fake" -------------
    WRITE_U8(I2CxCNTL, 0x7);
    WRITE_BOOL(I2CxGO, true);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    do { READ_U8(I2CxSTAT, &stat); } while (stat & 0x1);
    if (stat & 0x2) {
        throw runtime_error("i2c err: RX single byte");
    }
    sleep(1);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    READ_U8(I2CxDATI, &val);
    return val;
}
void I2CPort::WriteI2CSingleByte(byte _deviceChainNum, byte data) {
    locker lock(this);
    uint8_t stat;
    WRITE_U8(I2CxADDR, (_deviceChainNum << 1) | 0x0);
    WRITE_U8(I2CxDATO, data);
    WRITE_U8(I2CxCNTL, 0x7);
    WRITE_BOOL(I2CxGO, true);
    //============================================
    do { READ_U8(I2CxSTAT, &stat); } while (stat & 0x1);
    if (stat & 0x2) {
        throw runtime_error("i2c err: TX single byte");
    }
}
void I2CPort::WriteI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data, unsigned count, bool reverse) {
    locker lock(this);
    uint8_t stat;
    WRITE_U8(I2CxADDR, (_deviceChainNum << 1) | 0x0);
    //-------- ��������� ������ ��� ������ ������ -------------
    {
        WRITE_U8(I2CxDATO, address);
        WRITE_U8(I2CxCNTL, 0x3); // TX:START
        WRITE_BOOL(I2CxGO, true);
        //============================================
        do { READ_U8(I2CxSTAT, &stat); } while (stat & 0x1);
        if (stat & 0x2) {
            throw runtime_error("i2c err: TX first byte");
        }
        //============================================
    }
    //---------------------- ������ ---------------------------
    unsigned n = reverse ? count - 1 : 0;
    for (; reverse ? (n >= 1) : (n < count - 1); reverse ? n-- : n++) {
        WRITE_U8(I2CxDATO, data[n]);
        WRITE_U8(I2CxCNTL, 0x1); // TX
        WRITE_BOOL(I2CxGO, true);
        //============================================
        do { READ_U8(I2CxSTAT, &stat); } while (stat & 0x1);
        if (stat & 0x2) {
            throw runtime_error("i2c err: TX middle byte");
        }
        //============================================
    }
    //----------------- ��������� ���� -----------------------
    WRITE_U8(I2CxDATO, reverse ? data[0] : data[count - 1]);
    WRITE_U8(I2CxCNTL, 0x5); // TX:STOP
    WRITE_BOOL(I2CxGO, true);
    //============================================
    do { READ_U8(I2CxSTAT, &stat); } while (stat & 0x1);
    if (stat & 0x2) {
        throw runtime_error("i2c err: TX last byte");
    }
    //============================================
}
void I2CPort::ReadI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data, int count, bool reverse) {
    locker lock(this);
    byte val;
    uint8_t stat;
    //------- ����� ����� ��� ������ ������ - 1 ---------
    WRITE_U8(I2CxADDR, (_deviceChainNum << 1) | 0x0);
    WRITE_U8(I2CxDATO, address - 1);
    WRITE_U8(I2CxCNTL, 0x7);
    WRITE_BOOL(I2CxGO, true);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    do { READ_U8(I2CxSTAT, &stat); } while (stat & 0x1);
    sleep(5);
    if (stat & 0x2) {
        throw runtime_error("i2c err: TX address byte");
    }
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    //--------------- ����� ������ -------------------
    WRITE_U8(I2CxADDR, (_deviceChainNum << 1) | 0x1);
    //----------- ������ ������ ����-"fake" -------------
    WRITE_U8(I2CxCNTL, 0xB);
    WRITE_BOOL(I2CxGO, true);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    do { READ_U8(I2CxSTAT, &stat); } while (stat & 0x1);
    if (stat & 0x2) {
        throw runtime_error("i2c err: RX \'fake\' byte");
    }
    sleep(1);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    READ_U8(I2CxDATI, &val);
    
    //------ ������ ��������� ���������� N-1 ���� -------
    int n = reverse ? count - 1 : 0;
    for (; reverse ? (n >= 1) : (n < count - 1); reverse ? n-- : n++) {
        WRITE_U8(I2CxCNTL, 0x9);
        WRITE_BOOL(I2CxGO, true);
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        do { READ_U8(I2CxSTAT, &stat); } while (stat & 0x1);
        if (stat & 0x2) {
            throw runtime_error("i2c err: RX middle byte");
        }
        sleep(1);
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        READ_U8(I2CxDATI, &data[n]);
    }
    //------------- ������ ��������� ���� ------------------
    WRITE_U8(I2CxCNTL, 0x5);
    WRITE_BOOL(I2CxGO, true);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    do { READ_U8(I2CxSTAT, &stat); } while (stat & 0x1); // �������� �����������...
    //============================================
    if (stat & 0x2) {
        throw runtime_error("i2c err: RX last byte");
    }
    sleep(1);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    READ_U8(I2CxDATI, &(reverse ? data[0] : data[count - 1]));
}
void I2CPort::ResetPorts() {
    if (_A) delete _A;
    _A = nullptr;
    if (_B) delete _B;
    _B = nullptr;
    if (_C) delete _C;
    _C = nullptr;
}

template<class Fct>
inline void exec_transaction(Fct&& fct) {
    int resend_count = 3;
    send:
    try {
        std::forward<Fct>(fct)();
        if (resend_count != 3)
            logw("transaction completed with resend!");
    } catch (...) {
        if (resend_count == 0)
            throw;
        else {
            sleep(5);
            resend_count--;
            goto send;
        }
    }
}

byte I2CDevice::ReadI2CSingleByte() {
    byte b;
    exec_transaction([ this, &b ]() {
        b = _slot->ReadI2CSingleByte(_deviceChainNum);
    });
    return b;
}
void I2CDevice::WriteI2CSingleByte(byte data) {
    exec_transaction([ = ]() {
        _slot->WriteI2CSingleByte(_deviceChainNum, data);
    });
}
void I2CDevice::WriteI2CAddressedBytes(byte address, byte* data, unsigned count, bool reverse) {
    exec_transaction([ = ]() {
        _slot->WriteI2CAddressedBytes(_deviceChainNum, address, data, count, reverse);
    });
}
void I2CDevice::ReadI2CAddressedBytes(byte address, byte* data, int count, bool reverse) {
    exec_transaction([ = ]() {
        _slot->ReadI2CAddressedBytes(_deviceChainNum, address, data, count, reverse);
    });
}
I2CDevice::I2CDevice(byte deviceChainNum, II2CSlot* slot)
        : _deviceChainNum(deviceChainNum), _slot(slot) {}

void StandardI2CDevice::GetVersion(char buff[]) {
    ReadI2CAddressedBytes(Version + 1, (byte*) buff, 7);
    buff[4] = '\0';
}
void StandardI2CDevice::GetManufacturer(char buff[]) {
    ReadI2CAddressedBytes(Manufacturer, (byte*) buff, 8);
    buff[8] = '\0';
}
void StandardI2CDevice::GetDeviceName(char buff[]) {
    ReadI2CAddressedBytes(SensorType, (byte*) buff, 8);
    buff[8] = '\0';
}
StandardI2CDevice::StandardI2CDevice(byte deviceChainNum, II2CSlot* slot) : I2CDevice(deviceChainNum, slot) {}

MyRioLEDBoard::MyRioLEDBoard() {
    logi("LED Board: Init");
    switchOffAll();
}
MyRioLEDBoard::~MyRioLEDBoard() {
    logi("LED Board: Destruct");
    switchOffAll();
}
bool MyRioLEDBoard::led(int _led) {
    if (_led > 3 || _led < 0) throw std::out_of_range("led N is out of range!");
    uint8_t reg;
    READ_BOOL(DOLED30, &reg);
    return bool(reg & (0x1 << _led));
}
bool MyRioLEDBoard::led(int _led, bool enable) {
    if (_led > 3 || _led < 0) throw std::out_of_range("led N is out of range!");
    locker lock(this);
    uint8_t reg;
    READ_U8(DOLED30, &reg);
    if (enable)
        reg |= 0x1 << _led;
    else
        reg &= ~(0x1 << _led);
    WRITE_U8(DOLED30, reg);
    return enable;
}

void MyRioLEDBoard::switchOffAll() {
    locker lock(this);
    WRITE_U8(DOLED30, 0x0);
}
bool MyRioLEDBoard::toggle(int _led) {
    if (_led > 3 || _led < 0) throw std::out_of_range("led N is out of range!");
    locker lock(this);
    uint8_t reg;
    READ_U8(DOLED30, &reg);
    reg ^= 0x1 << _led;
    WRITE_U8(DOLED30, reg);
    return bool(reg & (0x1 << _led));
}
void MyRioLEDBoard::switchOnAll() {
    locker lock(this);
    WRITE_U8(DOLED30, 0b1111);
}
void MyRioLEDBoard::set(byte mask) {
    locker lock(this);
    WRITE_U8(DOLED30, mask);
}
std::shared_ptr<MyRioLEDBoard> MyRioLEDBoard::Instance(MyRioFpga&) {
    return singleton_instance<MyRioLEDBoard>([]() { return new MyRioLEDBoard(); });
}

I2CMultiplexor3::I2CMultiplexor3(II2CSlot* slot) : I2CDevice(0b01110111, slot) {
    try {
        _active_channel = ReadI2CSingleByte() & (byte) 0b111;
    } catch (std::runtime_error&) {
        loge("Failed to get config of multiplexor!");
        throw;
    }
    for (int i = 0; i < 3; ++i)
        slots[i] = new MultiplexorSlot(this, i);
}
I2CMultiplexor3::session I2CMultiplexor3::OpenSession(int channel) {
    if (channel < -1 || channel > 2)
        throw std::invalid_argument("channel");
    byte shifted = (channel == -1) ? (byte) 0x0 : ((byte) 0x1 << channel);
    locker lock = locker{this}; // Начать сеанс с выбранным каналом и заблокировать мультиплексор.
    // Даже если этот канал уже выбран, его выбор нужно заблокировать
    if (shifted != _active_channel) {
        try {
            WriteI2CSingleByte(shifted);
        } catch (std::runtime_error& e) {
            goto error;
        }
        _active_channel = ReadI2CSingleByte() & (byte) 0b111;
        if (_active_channel != shifted) {
            error:
            // Сеанс начать не удалось - разблокируем мультиплексор.
            throw std::runtime_error("failed to switch i2c channel!");
        }
    }
    return lock;
}
I2CMultiplexor3::~I2CMultiplexor3() {
    for (int i = 0; i < 3; ++i)
        delete slots[i];
}

using float_t_ = MyRioAccelerometer::float_t;
inline float_t_ _get_accel(regist reg) {
    uword val;
    READ_U16(reg, &val);
    return float_t_(word(val)) / float_t_(256.0);
}
float_t_ MyRioAccelerometer::get_x_accel() { return _get_accel(ACCXVAL); }
float_t_ MyRioAccelerometer::get_y_accel() { return _get_accel(ACCYVAL); }
float_t_ MyRioAccelerometer::get_z_accel() { return _get_accel(ACCZVAL); }
std::shared_ptr<MyRioAccelerometer> MyRioAccelerometer::Instance(MyRioFpga& must_init) {
    return singleton_instance<MyRioAccelerometer>([]() { return new MyRioAccelerometer(); });
}

SynchronizedDevice::locker::locker(SynchronizedDevice* device)
        : _mutex(&device->_mutex) {
    pthread_mutex_lock(_mutex);
}
SynchronizedDevice::locker::~locker() {
    if (_mutex)
        pthread_mutex_unlock(_mutex);
}
SynchronizedDevice::locker::locker(SynchronizedDevice::locker&& rh) {
    _mutex = rh._mutex;
    rh._mutex = 0;
}

#define IRQ_NUM 3
MyRioButton::MyRioButton() {
    _event = Release;
    NiFpga_Status status;
    status = NiFpga_ReserveIrqContext(myrio_session, &_irq_context);
    if (MyRio_IsNotSuccess(status)) throw runtime_error("can't register IRQ context");
    WRITE_U8(IRQDI_BTNNO, IRQ_NUM);
    WRITE_U32(IRQDI_BTNCNT, 1);
    WRITE_BOOL(IRQDI_BTNENA, true);
    _config();
    //
    _watching = true;
    _watcher = task([ this ]() {
        logi("started button watcher.");
        uint32_t occured;
        while (_watching) {
            occured = 0;
            NiFpga_WaitOnIrqs(myrio_session, _irq_context, 1 << IRQ_NUM, 50, &occured, nullptr);
            if (occured) {
                locker lock{this};
                action();
                NiFpga_AcknowledgeIrqs(myrio_session, occured);
            }
            sleep(2);
        }
        logi("ended button watcher.");
    });
}
void MyRioButton::setAction(const std::function<void()>& act, MyRioButton::Event event) {
    locker lock{this};
    action = std::move(act);
    _event = event;
    _config();
}
void MyRioButton::_config() {
    int isPress = _event & Press;
    int isRelease = _event & Release;
    WRITE_BOOL(IRQDI_BTNRISE, (bool) isPress);
    WRITE_BOOL(IRQDI_BTNFALL, (bool) isRelease);
}
MyRioButton::~MyRioButton() {
    _watching = false;
    _watcher.join();
    WRITE_BOOL(IRQDI_BTNENA, false);
    NiFpga_UnreserveIrqContext(myrio_session, _irq_context);
}
std::shared_ptr<MyRioButton> MyRioButton::Instance(MyRioFpga& must_init) {
    return singleton_instance<MyRioButton>([]() { return new MyRioButton(); });
}
byte I2CMultiplexor3::MultiplexorSlot::ReadI2CSingleByte(byte _deviceChainNum) {
    auto session_lock = _gate->OpenSession(_channel);
    return _gate->GetSlot()->ReadI2CSingleByte(_deviceChainNum);
}
void I2CMultiplexor3::MultiplexorSlot::WriteI2CSingleByte(byte _deviceChainNum, byte data) {
    auto session_lock = _gate->OpenSession(_channel);
    _gate->GetSlot()->WriteI2CSingleByte(_deviceChainNum, data);
}
void I2CMultiplexor3::MultiplexorSlot::WriteI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data,
                                                              unsigned count, bool reverse) {
    auto session_lock = _gate->OpenSession(_channel);
    _gate->GetSlot()->WriteI2CAddressedBytes(_deviceChainNum, address, data, count, reverse);
}
void I2CMultiplexor3::MultiplexorSlot::ReadI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data,
                                                             int count, bool reverse) {
    auto session_lock = _gate->OpenSession(_channel);
    _gate->GetSlot()->ReadI2CAddressedBytes(_deviceChainNum, address, data, count, reverse);
}
I2CMultiplexor3::MultiplexorSlot::MultiplexorSlot(I2CMultiplexor3* gate, int channel)
        : _gate(gate), _channel(channel) {}
}