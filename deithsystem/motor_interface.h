//
// Created by marco on 29/01/17.
//

#ifndef PROJECT_MOTOR_INTERFACE_H
#define PROJECT_MOTOR_INTERFACE_H

#include "util.h"

namespace deithvenbot {
/**
 * @brief
 */
class IPreciseServoMotor {
public:
    virtual float GetPosition() const =0;
    virtual void SetPosition(float pos)=0;
    virtual void AdvancePosition(float offset)=0;
    //! \brief Set speed for servo-motor
    //! \param timing [0;15]. 0 - immediate(very fast) .. 15 - very slow
    virtual void SetTiming(byte timing)=0;
protected:
    virtual ~IPreciseServoMotor(){};
};
}

#endif //PROJECT_MOTOR_INTERFACE_H
