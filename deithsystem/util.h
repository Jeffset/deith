/*
 * ------------------ util.h ----------------------------
 * Некоторые глобальные общеупотребительные программные объекты
 */

#ifndef PROJECT_UTIL_H
#define PROJECT_UTIL_H

#include <cstdint>
#include <string>

namespace deithvenbot {

// Глобальные псевдонимы типов

using byte = std::uint8_t;
using sbyte = std::int8_t;
using uword = std::uint16_t;
using word = std::int16_t;
using regist =  std::uint32_t;

/*
 * Макросы для общения с NI MyRIO FPGA (для использования необходимо определить
 * extern NiFpga_Session myrio_session;
 */

// TODO Make READ/WRITE fpga macro safe (mb inline functions)

#define READ_BOOL(reg, val)    NiFpga_ReadBool        (myrio_session, reg, val)
#define READ_U8(reg, val)    NiFpga_ReadU8    (myrio_session, reg, val)
#define READ_U16(reg, val)    NiFpga_ReadU16    (myrio_session, reg, val)

#define WRITE_BOOL(reg, val)NiFpga_WriteBool    (myrio_session, reg,(int)(val))
#define WRITE_U8(reg, val)    NiFpga_WriteU8    (myrio_session, reg, val)
#define WRITE_U16(reg, val) NiFpga_WriteU16    (myrio_session, reg, val)
#define WRITE_U32(reg, val) NiFpga_WriteU32    (myrio_session, reg, val)
#define WRITE_I16(reg, val) NiFpga_WriteI16    (myrio_session, reg, val)

extern bool use_colored_logging;

// Функции логирования
//! \brief  Логирует сообщение
void logm(const char* msg);
void logm(const std::string& msg);
//! \brief  Логирует важную инфу (с выделением)
void logi(const char* info);
void logi(const std::string& info);
//! \brief Логирует сообщение об успехе
void logok(const char* succ);
void logok(const std::string& msg);
//! \brief  Логирует предупреждение
void logw(const char* warn);
//! \brief Логирует ошибку
void loge(const char* err);
void loge(const std::string& err);

//! \brief Класс для измерения промежутков времени в миллисекундах
class Timer {
    class _Timer_impl;
    _Timer_impl* _impl;
public:
    //! \brief Создает объект таймера и начинает фиксирует начальное время
    Timer();
    //! \brief сбрасывает начальное время на текущее
    void restart();
    //! \brief возвращет прошедшее время с момента создания либо последнего restart()
    //! \return время в миллисекундах
    long get_elapsed_ms() const;
    ~Timer();
};

//! \brief Блокирует выполнения на определенное время
//! \param ms Время в миллисекундах
void sleep(int ms);
void sleep_ns(int ns);

constexpr int operator "" _s(unsigned long long sec) {
    return (int) (sec * 1000);
}
std::string operator "" _str(const char* str, unsigned int);
}

#endif //PROJECT_UTIL_H
