#include"servo.h"

extern NiFpga_Session myrio_session;

namespace deithvenbot {

float FixDown = 128.0f;
float HighUp = 90;
float LowUp = 100;
float SlowestUp = 115;
float SlowestDown = 160;
float LowDown = 163;
float HighDown = 180;

void RioControlServo(MyRioPort port, int pwmN, float angle) {
    NiFpga_Status status;
    uint32_t PWM_CNFG, PWM_CS, PWM_MAX, PWM_CMP;
    uint8_t portSelect;
    switch (port) {
        case MyRioPort::A:
            READ_U8(SYSSELECTA, &portSelect);
            switch (pwmN) {
                case 0:
                    PWM_CNFG = PWMA_0CNFG;
                    PWM_CS = PWMA_0CS, PWM_MAX = PWMA_0MAX, PWM_CMP = PWMA_0CMP;
                    status = WRITE_U8(SYSSELECTA, portSelect | 0x4);
                    break;
                case 1:
                    PWM_CNFG = PWMA_1CNFG;
                    PWM_CS = PWMA_1CS, PWM_MAX = PWMA_1MAX, PWM_CMP = PWMA_1CMP;
                    status = WRITE_U8(SYSSELECTA, portSelect | 0x8);
                    break;
                case 2:
                    PWM_CNFG = PWMA_2CNFG;
                    PWM_CS = PWMA_2CS, PWM_MAX = PWMA_2MAX, PWM_CMP = PWMA_2CMP;
                    status = WRITE_U8(SYSSELECTA, portSelect | 0x10);
                    break;
                default:
                    throw std::out_of_range("PWM number out of range!");
            }
            break;
        case MyRioPort::B:
            READ_U8(SYSSELECTB, &portSelect);
            switch (pwmN) {
                case 0:
                    PWM_CNFG = PWMB_0CNFG;
                    PWM_CS = PWMB_0CS, PWM_MAX = PWMB_0MAX, PWM_CMP = PWMB_0CMP;
                    status = WRITE_U8(SYSSELECTB, portSelect | 0x4);
                    break;
                case 1:
                    PWM_CNFG = PWMB_1CNFG;
                    PWM_CS = PWMB_1CS, PWM_MAX = PWMB_1MAX, PWM_CMP = PWMB_1CMP;
                    status = WRITE_U8(SYSSELECTB, portSelect | 0x8);
                    break;
                case 2:
                    PWM_CNFG = PWMB_2CNFG;
                    PWM_CS = PWMB_2CS, PWM_MAX = PWMB_2MAX, PWM_CMP = PWMB_2CMP;
                    status = WRITE_U8(SYSSELECTB, portSelect | 0x16);
                    break;
                default:
                    throw std::out_of_range("PWM number out of range!");
            }
            break;
        case MyRioPort::C:
            READ_U8(SYSSELECTC, &portSelect);
            switch (pwmN) {
                case 0:
                    PWM_CNFG = PWMC_0CNFG;
                    PWM_CS = PWMC_0CS, PWM_MAX = PWMC_0MAX, PWM_CMP = PWMC_0CMP;
                    status = WRITE_U8(SYSSELECTC, portSelect | 0x2);
                    break;
                case 1:
                    PWM_CNFG = PWMC_1CNFG;
                    PWM_CS = PWMC_1CS, PWM_MAX = PWMC_1MAX, PWM_CMP = PWMC_1CMP;
                    status = WRITE_U8(SYSSELECTC, portSelect | 0x8);
                    break;
                default:
                    throw std::out_of_range("PWM number out of range!");
            }
            break;
        default:
            throw std::out_of_range("Invalid port value!");
    }
    if (status) throw std::runtime_error("Wasn't able to config PWM via SYSSELECT register!");
    
    uint16_t val = uint16_t(3.4179f * angle + 498.046f);
    status = WRITE_U8(PWM_CNFG, 0x4);
    if (status) throw std::runtime_error("Wasn't able to write PWM_CNFG register!");
    status = WRITE_U8(PWM_CS, 0x7);
    if (status) throw std::runtime_error("Wasn't able to write PWM_CS register!");
    status = WRITE_U16(PWM_MAX, 12500);
    if (status) throw std::runtime_error("Wasn't able to write PWM_MAX register!");
    status = WRITE_U16(PWM_CMP, val);
    if (status) throw std::runtime_error("Wasn't able to write PWM_CMP register!");
}

using namespace std;
enum ServoDriverAddr {
    Status = 0x40,
    StepTime = 0x41,
    ServoPosBase = 0x42 - 1,
    EnablePWM = 0x48
};

ServoDriverHT::ServoDriverHT(byte deviceChainNum, II2CSlot* slot)
        : StandardI2CDevice(deviceChainNum, slot) {}

void ServoDriverHT::SetServoValue(int servo, float pos) {
    if (servo < 1 || servo > 6)
        throw std::invalid_argument("invalid servo");
    WriteI2CData(ServoPosBase + servo, static_cast<byte>(pos));
    sleep(50);
    while (IsBusy())
        sleep(5);
}
void ServoDriverHT::SetStepTime(byte time) {
    if (time > 15)
        throw std::invalid_argument("not supported time");
    WriteI2CData(StepTime, time);
}
void ServoDriverHT::Enable(bool enabled) {
    WriteI2CData(EnablePWM, byte(enabled ? 0xAA : 0xFF));
}
bool ServoDriverHT::Enabled() {
    byte epwm = ReadI2CData<byte>(EnablePWM);
    switch (epwm) {
        case 0x00:
        case 0xAA:
            return true;
        case 0xFF:
            return false;
        default:
            throw std::runtime_error("Invalid EnablePWM value was read!");
    }
}
bool ServoDriverHT::IsBusy() {
    return ReadI2CData<bool>(Status);
}
byte ServoDriverHT::GetStepTime() {
    return ReadI2CData<byte>(StepTime);
}
float ServoDriverHT::GetServoValue(int servo) {
    if (servo < 1 || servo > 6)
        throw std::invalid_argument("invalid servo");
    return ReadI2CData<byte>(ServoPosBase + servo);
}
ServoDriverHT::~ServoDriverHT() {
    Enable(false);
}

float PreciseServo::GetPosition() const { return _driver->GetServoValue(_channel); }
void PreciseServo::SetPosition(float pos) {
    _driver->SetStepTime(_timing);
    _driver->SetServoValue(_channel, pos); }

void PreciseServo::AdvancePosition(float offset) {
    float cur = GetPosition();
    cur += offset;
    cur = cur < 0 ? 0 : (cur > 255 ? 255 : cur);
    SetPosition(cur);
}

void PreciseServo::SetTiming(byte timing) {
    _timing = timing;
}
StandardServo::StandardServo(IServoDriver* driver, int channel)
        : _driver(driver), _channel(channel) {
    if (!_driver)
        throw std::invalid_argument("driver");
}

ContinuousServo::ContinuousServo(IServoDriver* driver, int channel)
        : StandardServo(driver, channel) {
    Stop();
}
void ContinuousServo::StartMove(float speed) {
    _driver->SetStepTime(0);
    _driver->SetServoValue(_channel, (speed));
}
void ContinuousServo::Stop() {
    _driver->SetServoValue(_channel, FixDown);
}
float ContinuousServo::GetSpeed() const {
    return _driver->GetServoValue(_channel);
}

bool ServoDriverPWM::IsBusy() {
    return false; // Always 'free'
}

byte ServoDriverPWM::GetStepTime() {
    // TODO get step time (now not used to control position servo thus not supported)
    return 0;
}

void ServoDriverPWM::SetStepTime(byte) {
    // TODO set step time (now not used to control position servo thus not supported)
}

float ServoDriverPWM::GetServoValue(int servo) {
    return pwmRegists[servo].value;
}

void ServoDriverPWM::SetServoValue(int servo, float pos) {
    NiFpga_Status status;
    uint16_t val = uint16_t(3.4179f * pos /* *255 */ + 498.046f);
    status = WRITE_U16(pwmRegists[servo].PWM_CMP, val);
    if (status) throw std::runtime_error("Wasn't able to write PWM_CMP register!");
    pwmRegists[servo].value = pos;
}

ServoDriverPWM::ServoDriverPWM(MyRioPort port) {
    _port = port;
    //
    switch (_port) {
        case MyRioPort::A:
            SYSSELECT = SYSSELECTA;
            pwmRegists[0].PWM_CNFG = PWMA_0CNFG;
            pwmRegists[0].PWM_CS = PWMA_0CS, pwmRegists[0].PWM_MAX = PWMA_0MAX, pwmRegists[0].PWM_CMP = PWMA_0CMP;
            
            pwmRegists[1].PWM_CNFG = PWMA_1CNFG;
            pwmRegists[1].PWM_CS = PWMA_1CS, pwmRegists[1].PWM_MAX = PWMA_1MAX, pwmRegists[1].PWM_CMP = PWMA_1CMP;
            
            pwmRegists[2].PWM_CNFG = PWMA_2CNFG;
            pwmRegists[2].PWM_CS = PWMA_2CS, pwmRegists[2].PWM_MAX = PWMA_2MAX, pwmRegists[2].PWM_CMP = PWMA_2CMP;
            supported_pwms = 3;
            break;
        case MyRioPort::B:
            SYSSELECT = SYSSELECTB;
            pwmRegists[0].PWM_CNFG = PWMB_0CNFG;
            pwmRegists[0].PWM_CS = PWMB_0CS, pwmRegists[0].PWM_MAX = PWMB_0MAX, pwmRegists[0].PWM_CMP = PWMB_0CMP;
            
            pwmRegists[1].PWM_CNFG = PWMB_1CNFG;
            pwmRegists[1].PWM_CS = PWMB_1CS, pwmRegists[1].PWM_MAX = PWMB_1MAX, pwmRegists[1].PWM_CMP = PWMB_1CMP;
            
            pwmRegists[2].PWM_CNFG = PWMB_2CNFG;
            pwmRegists[2].PWM_CS = PWMB_2CS, pwmRegists[2].PWM_MAX = PWMB_2MAX, pwmRegists[2].PWM_CMP = PWMB_2CMP;
            supported_pwms = 3;
            break;
        case MyRioPort::C:
            SYSSELECT = SYSSELECTC;
            pwmRegists[0].PWM_CNFG = PWMC_0CNFG;
            pwmRegists[0].PWM_CS = PWMC_0CS, pwmRegists[0].PWM_MAX = PWMC_0MAX, pwmRegists[0].PWM_CMP = PWMC_0CMP;
            
            pwmRegists[1].PWM_CNFG = PWMC_1CNFG;
            pwmRegists[1].PWM_CS = PWMC_1CS, pwmRegists[1].PWM_MAX = PWMC_1MAX, pwmRegists[1].PWM_CMP = PWMC_1CMP;
            supported_pwms = 2;
            break;
        default:
            throw std::out_of_range("Invalid port value!");
    }
}

bool ServoDriverPWM::Enabled() {
    // TODO return actual "Enabled"
    return true;
}

void ServoDriverPWM::Enable(bool enable) {
    NiFpga_Status status;
    uint8_t portSelect;
    status = READ_U8(SYSSELECT, &portSelect);
    if (status) throw std::runtime_error("Wasn't able to read SYSSELECT register!");
    
    if (enable)
        for (int pwmN = 0; pwmN < supported_pwms; ++pwmN) {
            if (SYSSELECT == SYSSELECTC)
                status = WRITE_U8(SYSSELECT, portSelect | (0x1 << (2 * pwmN + 1)));
            else
                status = WRITE_U8(SYSSELECT, portSelect | (0x1 << (pwmN + 2)) /*| (0x1 << 7)*/);
            if (status) throw std::runtime_error("Wasn't able to write SYSSELECT register!");
            //
            status = WRITE_U8(pwmRegists[pwmN].PWM_CNFG, 0x4);
            if (status) throw std::runtime_error("Wasn't able to write PWM_CNFG register!");
            status = WRITE_U8(pwmRegists[pwmN].PWM_CS, 0x7);
            if (status) throw std::runtime_error("Wasn't able to write PWM_CS register!");
            status = WRITE_U16(pwmRegists[pwmN].PWM_MAX, 12500);
            if (status) throw std::runtime_error("Wasn't able to write PWM_MAX register!");
        }
    else {
        if (SYSSELECT == SYSSELECTC)
            status = WRITE_U8(SYSSELECT, portSelect & ~0xA);
        else
            status = WRITE_U8(SYSSELECT, portSelect & ~0x1C);
        if (status) throw std::runtime_error("Wasn't able to write SYSSELECT register!");
    }
}
ServoDriverPWM::~ServoDriverPWM() {
    Enable(false);
}
    
}
