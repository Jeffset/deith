#include <cstring>
#include <chrono>
#include <cmath>
#include "audio.h"

extern NiFpga_Session myrio_session;

namespace deithvenbot {

WavAudio::WavAudio(const std::string& filename) {
    in.open(filename, std::ios::in | std::ios::binary);
    if (!in.is_open())throw std::runtime_error("wasn't able to open sound file");
    // "RIFF"
    byte data[16];
    in.read((char*) data, 4);
    data[4] = '\0';
    if (std::strcmp((const char*) data, "RIFF") != 0)
        throw std::runtime_error("not a WAV file (RIFF missing)");
    // file size
    in.read((char*) data, 4);
    _fileSize = *reinterpret_cast<int*>(data);
    // "WAVEfmt "
    in.read((char*) data, 8);
    data[8] = '\0';
    if (std::strcmp((const char*) data, "WAVEfmt ") != 0)
        throw std::runtime_error("unsupported WAV file (WAVEfmt missing)");
    // 16 (pcm)
    in.read((char*) data, 4);
    int pcmConst = *reinterpret_cast<int*>(data);
    if (pcmConst != 16)
        throw std::runtime_error("only PCM wav is supported");
    // 1 (compression)
    in.read((char*) data, 2);
    short compression = *reinterpret_cast<short*>(data);
    if (compression != 1)
        throw std::runtime_error("compression is not supported");
    // channel count
    in.read((char*) data, 2);
    _channelCount = *reinterpret_cast<short*>(data);
    if (_channelCount > 2)
        throw std::runtime_error("2 maximum channels are supported");
    // sample rate
    in.read((char*) data, 4);
    _sampleRate = *reinterpret_cast<int*>(data);
    // bytes per second
    in.read((char*) data, 4);
    _bytePerSecond = *reinterpret_cast<int*>(data);
    // bytes per one sample
    in.read((char*) data, 2);
    _bytePerSample = *reinterpret_cast<short*>(data);
    // bits per channel in sample
    in.read((char*) data, 2);
    _bitPerChannel = *reinterpret_cast<short*>(data);
    // "data"
    in.read((char*) data, 4);
    data[4] = '\0';
    if (std::strcmp((const char*) data, "data") != 0)
        throw std::runtime_error("\"data\" header expected");
    // data size in bytes
    in.read((char*) data, 4);
    _dataSize = *reinterpret_cast<int*>(data);
    // next goes data;
    _dataStreamBeginPos = in.tellg();
    //
    _totalSamplesCount = _dataSize / _bytePerSample;
}
void WavAudio::nextSample(ushort samples[2]) {
    char* data = (char*) samples;
    in.read(data, _bytePerSample);
    ++currentSample;
}
bool WavAudio::endOfStream() const {
    return currentSample == _totalSamplesCount - 1;
}
int WavAudio::getSampleRate() const {
    return _sampleRate;
}
void WavAudio::resetStream() {
    in.seekg(_dataStreamBeginPos);
    currentSample = 0;
}
short WavAudio::getChannelCount() const {
    return _channelCount;
}
MyRioAudioSystem::MyRioAudioSystem() {
    running = true;
    audio_service = task([ this ]() {
        logi("audio: service: started.");
        auto audio_go = std::chrono::high_resolution_clock::now();
        short channelCount;
        ushort samples[2];
        int ns_count;
        while (running) {
            if (_playing) {
                locker lock(this);
                if (_currentPlaying->endOfStream()) {
                    logi("audio: service: stream end.");
                    _playing = false;
                    continue;
                }
                ns_count = 1'000'000'000 / _currentPlaying->getSampleRate();;
                channelCount = _currentPlaying->getChannelCount();
                _currentPlaying->nextSample(samples);
            }
            else {
                sleep(5);
                audio_go = std::chrono::high_resolution_clock::now();
                continue;
            }
            WRITE_U16(AOAudioOut_LVAL, samples[0]);
            WRITE_U16(AOAudioOut_RVAL, channelCount == 2 ? samples[1] : samples[0]);
            while (true) {
                auto now = std::chrono::high_resolution_clock::now();
                if ((now - audio_go).count() >= ns_count - 3000)break;
            }
            /*int val = 1250 * ns_count / 22676;
            for (int i = 0; i < val; ++i) {
                asm("nop");
            }*/
            WRITE_BOOL(AOSYSGO, (uint8_t) true);
            audio_go = std::chrono::high_resolution_clock::now();
        }
        logi("audio: service: finished.");
    });
}
std::shared_ptr<IAudioStream> MyRioAudioSystem::getAudio() const {
    return _currentPlaying;
}
void MyRioAudioSystem::play(std::shared_ptr<IAudioStream> audio) {
    locker lock(this);
    _currentPlaying = audio;
    audio->resetStream();
    _playing = true;
    logi("audio: resume stream.");
}
MyRioAudioSystem::~MyRioAudioSystem() {
    {
        locker lock(this);
        running = false;
    }
    audio_service.join();
}
void MyRioAudioSystem::resume() {
    if (_currentPlaying != nullptr && !_playing) {
        locker lock(this);
        _playing = true;
    }
}
void MyRioAudioSystem::stop() {
    if (_currentPlaying != nullptr && _playing) {
        locker lock(this);
        _playing = false;
    }
}
std::shared_ptr<MyRioAudioSystem> MyRioAudioSystem::Instance(MyRioFpga& must_init) {
    return singleton_instance<MyRioAudioSystem>([]() { return new MyRioAudioSystem(); });
}

void SineAudio::nextSample(ushort* sample) {
    sample[0] = (ushort) (std::sin(argument) * 65536);
    argument += 3.1415 * frequency / 44100.0f;
}
bool SineAudio::endOfStream() const {
    return false;
}
int SineAudio::getSampleRate() const {
    return 44100;
}
short SineAudio::getChannelCount() const {
    return 1;
}
void SineAudio::resetStream() {
    argument = 0;
}
SineAudio::SineAudio(float frequency) : frequency(frequency) {
    
}
}
