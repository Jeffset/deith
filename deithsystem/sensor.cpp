#include "sensor.h"
#include <sstream>

extern NiFpga_Session myrio_session;

namespace deithvenbot {
const char* color_names[] = {
        "Black",
        "Violet",
        "VioletBlue",
        "Blue",
        "Green",
        "GreenYellow",
        "Yellow",
        "Orange",
        "Red",
        "Purple",
        "Magenta",
        "PaleViolet",
        "PaleYellowish",
        "PaleYellow",
        "PaleOrange",
        "PaleRed",
        "PaleMagenta",
        "White",
        "None"
};

enum HiTechColSensorDriverAddr {
    Command = 0x41,
    ColorNumber = 0x42,
    RedReading = 0x43,
    GreenReading = 0x44,
    BlueReading = 0x45
};

enum EV3ColSensorAddr {
    EV3SensorMode = 0x52,
    EV3SensorData = 0x54
};

ColorEnum HiTechColorSensor::GetEnumedColor() {
    //return ColorEnum(_driver->GetEnumedColor(_gate_channel));
    return (ColorEnum) ReadI2CData<byte>(ColorNumber);
}
RGBColor HiTechColorSensor::GetRGB() {
    /*byte
            r = _driver->Get_R(_gate_channel),
            g = _driver->Get_G(_gate_channel),
            b = _driver->Get_B(_gate_channel);
    return RGBColor(r, g, b);*/
    RGBColor color;
    byte data[3];
    ReadI2CAddressedBytes(RedReading, data, 3);
    color.R = data[0];
    color.G = data[1];
    color.B = data[2];
    return color;
}
HiTechColorSensor::HiTechColorSensor(II2CSlot* slot)
        : StandardI2CDevice(0x1, slot) {}
float HiTechColorSensor::GetLightness() {
    auto rgb = GetRGB();
    return float(rgb.R + rgb.G + rgb.B) / 3.f;
}

std::string RGBColor::getStr() {
    std::ostringstream oss;
    oss << '{' << (int) R << ", " << (int) G << ", " << (int) B << "}";
    return oss.str();
}
AnalogSensor::AnalogSensor(int _channel, MyRioPort port)
        : _channel(_channel), port(port), _zero(2900), _one(2299) {
    if (_channel < 0 || _channel > 2)
        throw std::out_of_range("channel");
    switch (port) {
        case MyRioPort::A:
            SYSSELECT = SYSSELECTA;
            switch (_channel) {
                case 0:
                    AI = AIA_0VAL;
                    break;
                case 1:
                    AI = AIA_1VAL;
                    break;
                case 2:
                    AI = AIA_2VAL;
                    break;
            }
            break;
        case MyRioPort::B:
            SYSSELECT = SYSSELECTB;
            switch (_channel) {
                case 0:
                    AI = AIB_0VAL;
                    break;
                case 1:
                    AI = AIB_1VAL;
                    break;
                case 2:
                    AI = AIB_2VAL;
                    break;
            }
            break;
        default:
            throw std::invalid_argument("port is invalid or not supported!");
    }
}
double AnalogSensor::RetrieveRawValue() {
    uint16_t val;
    READ_U16(AI, &val);
    return val;
}

double _Calibrate(AnalogSensor& analog) {
    constexpr int ITER = 40;
    double value = 0.0f;
    for (int i = 0; i < ITER; ++i) {
        value += analog.RetrieveRawValue();
        sleep(20);
    }
    return value / ITER;
}
void AnalogSensor::CalibrateZero() {
    _zero = _Calibrate(*this);
}
void AnalogSensor::CalibrateOne() {
    _one = _Calibrate(*this);
}
float AnalogSensor::GetLightness() {
    auto fctor = [ this ]() -> double { return 1.0 - (RetrieveRawValue() - _one) / (_zero - _one); };
    constexpr int SAMPLE_COUNT = 3;
    auto val = 0.0;
    for (int i = 0; i < SAMPLE_COUNT; ++i) {
        val += fctor();
        sleep(1);
    }
    return float(val / SAMPLE_COUNT);
}
ColorEnum AnalogSensor::GetEnumedColor() {
    return GetLightness() < 0.5 ? Black : White;
}
ColorEnum AnalogSensor::Wait(WaitColorMode mode, WaitColor waitStruct, int timeout) {
    if ((waitStruct.from != Black && waitStruct.from != White) ||
        (waitStruct.to != Black && waitStruct.to != White))
        throw std::invalid_argument("color not supported!");
    return AbstractColorSensor::Wait(mode, waitStruct, timeout);
}
int AnalogSensor::Test() {
    auto rawVal = RetrieveRawValue();
    if (rawVal < 300) {
        return -1;
    }
    else if (rawVal > 3900) {
        loge("Too high analog value!");
        return +1;
    }
    else
        return 0;
}

const char* get_color_name(ColorEnum value) {
    if (value < Black || value > White)
        throw std::invalid_argument("value is not color!");
    return color_names[(int) value];
}
ColorEnum get_color_value_by_name(const std::string name) {
    for (int i = 0; i < (None + 1); ++i) {
        if (std::string(color_names[i]) == name)
            return (ColorEnum) i;
    }
    throw std::invalid_argument("invalid color name!");
}
const char* AbstractColorSensor::GetEnumedColorName() {
    return color_names[(int) GetEnumedColor()];
}
ColorEnum AbstractColorSensor::Wait(WaitColorMode mode, WaitColor waitStruct, int timeout) {
    if (timeout <= 0 && timeout != -1)
        throw std::invalid_argument("timeout");
    ColorEnum current;
    Timer timer;
    switch (mode) {
        case UntilColor:
            do {
                current = GetEnumedColor();
                sleep(10);
            } while (
                    (current < waitStruct.from ||
                     current > waitStruct.to) &&
                    (timeout == -1 ? true : timer.get_elapsed_ms() < timeout));
            break;
        case WhileColor:
            do {
                current = GetEnumedColor();
                sleep(10);
            } while (
                    (current >= waitStruct.from &&
                     current <= waitStruct.to) &&
                    (timeout == -1 ? true : timer.get_elapsed_ms() < timeout)
                    );
            break;
        default:
            throw std::invalid_argument("mode");
    }
    return current;
}
void AbstractColorSensor::WaitLightness(bool white, int timeout) {
    if (timeout <= 0 && timeout != -1)
        throw std::invalid_argument("timeout");
    Timer timer;
    float current;
    do {
        current = GetLightness();
        sleep(5);
    } while (white ? (current < 0.5) : (current >= 0.5) &&
                                       (timeout == -1 ? true : timer.get_elapsed_ms() < timeout));
}

ColorEnum EV3ColorSensor::GetEnumedColor() {
    switchMode(ev3Color);
    switch (ReadI2CData<uword>(EV3SensorData)) {
        case 0:
            return ColorEnum::None;
        case 1:
            return ColorEnum::Black;
        case 2:
            return ColorEnum::Blue;
        case 3:
            return ColorEnum::Green;
        case 4:
            return ColorEnum::Yellow;
        case 5:
            return ColorEnum::Red;
        case 6:
            return ColorEnum::White;
        case 7:
            return ColorEnum::Orange;
        default:
            throw std::runtime_error("strange color value!");
    }
}
float EV3ColorSensor::GetLightness() {
    switchMode(ev3ReflectedLight);
    return float(ReadI2CData<uword>(EV3SensorData)) / 100.f * 1.5f;
}
float EV3ColorSensor::getAmbiance() {
    switchMode(ev3AmbientLight);
    return float(ReadI2CData<uword>(EV3SensorData)) / 100.f;
}
EV3ColorSensor::EV3ColorSensor(II2CSlot* slot)
        : EV3Sensor(slot) {}
EV3GyroSensor::EV3GyroSensor(II2CSlot* slot)
        : EV3Sensor(slot) {}
int EV3GyroSensor::getRotation() {
    switchMode(ev3Angle);
    return ReadI2CData<word>(EV3SensorData);
}
int EV3GyroSensor::getRate() {
    switchMode(ev3Rate);
    return ReadI2CData<word>(EV3SensorData);
}
void EV3GyroSensor::resetGyro() {
    for (int i = 0; i < 2; ++i) {
        WriteI2CData<word>(EV3SensorMode, ev3Angle);
        WriteI2CData<word>(EV3SensorMode, ev3Rate);
        sleep(1000);
    }
    WriteI2CData<word>(EV3SensorMode, ev3Angle);
    currentMode = Unknown;
}

EV3Sensor::EV3Sensor(II2CSlot* slot) : StandardI2CDevice(0x19, slot) {}
void EV3Sensor::switchMode(word mode) {
    if (currentMode == Unknown || currentMode != mode) {
        WriteI2CData(EV3SensorMode, mode);
        currentMode = ReadI2CData<word>(EV3SensorMode);
    }
}
}