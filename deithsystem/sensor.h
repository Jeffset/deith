//======================= device.h ============================
/**************************************************************
***************************************************************/
#ifndef _BEDPO_SENSOR_
#define _BEDPO_SENSOR_

#include "device.h"

namespace deithvenbot {
///------------------------------------------------------------------------
//====================== SESNSOR SYSTEM =====================================
///------------------------------------------------------------------------


enum ColorEnum {
    Black = 0,
    Violet = 1,
    VioletBlue = 2,
    Blue = 3,
    Green = 4,
    GreenYellow = 5,
    Yellow = 6,
    Orange = 7,
    Red = 8,
    Purple = 9,
    Magenta = 10,
    PaleViolet = 11,
    PaleYellowish = 12,
    PaleYellow = 13,
    PaleOrange = 14,
    PaleRed = 15,
    PaleMagenta = 16,
    White = 17,
    None = 18,
};

struct RGBColor {
    byte R, G, B;
    RGBColor(byte r, byte g, byte b)
            : R(r), G(g), B(b) {}
    RGBColor() = default;
    std::string getStr();
};

enum WaitColorMode {
    UntilColor,
    WhileColor,
};
struct WaitColor {
    ColorEnum from, to;
    WaitColor(ColorEnum color)
            : from(color), to(color) {};
    WaitColor(ColorEnum f, ColorEnum t)
            : from(f), to(t) {};
};

class AbstractColorSensor {
public:
    virtual ColorEnum GetEnumedColor() = 0;
    virtual float GetLightness() = 0;
    const char* GetEnumedColorName();
    virtual ColorEnum Wait(WaitColorMode mode, WaitColor waitStruct, int timeout = -1);
    virtual void WaitLightness(bool white, int timeout = -1);
    virtual ~AbstractColorSensor(){};
};

class AnalogSensor : public AbstractColorSensor {
    regist SYSSELECT;
    regist AI;
    int _channel;
    MyRioPort port;
    //
    double _zero;
    double _one;
public:
    AnalogSensor(int _channel, MyRioPort port);
    void CalibrateZero();
    void CalibrateOne();
    double RetrieveRawValue();
    float GetLightness() override;
    virtual ColorEnum Wait(WaitColorMode mode, WaitColor waitStruct, int timeout) override;
    virtual ColorEnum GetEnumedColor() override;
    double getZero() const { return _zero; }
    double getOne() const { return _one; }
    int Test();
};

class HiTechColorSensor : public StandardI2CDevice, public AbstractColorSensor {
public:
    HiTechColorSensor(II2CSlot* slot);
    ColorEnum GetEnumedColor();
    float GetLightness() override;
    RGBColor GetRGB();
};
const char* get_color_name(ColorEnum value);
ColorEnum get_color_value_by_name(const std::string name);

class EV3Sensor : public StandardI2CDevice {
    static constexpr word Unknown = -1;
protected:
    word currentMode = Unknown;
protected:
    EV3Sensor(II2CSlot* slot);
    void switchMode(word mode);
};

class EV3ColorSensor : public EV3Sensor, public AbstractColorSensor {
private:
    enum EV3Mode : word {
        ev3Color = 2, ev3ReflectedLight = 0, ev3AmbientLight = 1
    };
public:
    EV3ColorSensor(II2CSlot* slot);
    virtual ColorEnum GetEnumedColor() override;
    float GetLightness() override;
    float getAmbiance();
};

class EV3GyroSensor : public EV3Sensor {
private:
    enum EV3Mode : word {
        ev3Angle = 3, ev3Rate = 1, Unknown = -1
    };
public:
    EV3GyroSensor(II2CSlot* slot);
    int getRotation();
    int getRate();
    void resetGyro();
};
    
}
#endif