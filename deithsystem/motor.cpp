#include "motor.h"

namespace deithvenbot {
using namespace std;
enum DCMotorDiverAddr {
    M1TargetEncoder = 0x40,
    M1Mode = 0x44,
    M1Power = 0x45,
    M2Power = 0x46,
    M2Mode = 0x47,
    M2TargetEncoder = 0x48,
    M1CurrentEncoder = 0x4C,
    M2CurrentEncoder = 0x50,
    BatteryVoltage = 0x54
};
DCMotorsDriver::DCMotorsDriver(byte deviceChainNum, II2CSlot* slot)
        : StandardI2CDevice(deviceChainNum, slot) {
    SetNoTimeout(true);
}
DCMotorsDriver::~DCMotorsDriver() {}
byte DCMotorsDriver::_GetMode(Motor motor) {
    switch (motor) {
        case Motor::M1:
            return ReadI2CData<byte>(M1Mode);
        case Motor::M2:
            return ReadI2CData<byte>(M2Mode);
        default:
            throw std::invalid_argument("motor");
    }
}
void DCMotorsDriver::_SetMode(Motor motor, byte mode) {
    switch (motor) {
        case Motor::M1:
            WriteI2CData(M1Mode, mode);
            break;
        case Motor::M2:
            WriteI2CData(M2Mode, mode);
            break;
        default:
            throw std::invalid_argument("motor");
    }
}
void DCMotorsDriver::SetNoTimeout(bool nto) {
    byte m = _GetMode(Motor::M1);
    if (nto) m |= (0x1 << 4); else m &= ~(0x1 << 4);
    _SetMode(Motor::M1, m);
    m = _GetMode(Motor::M2);
    if (nto) m |= (0x1 << 4); else m &= ~(0x1 << 4);
    _SetMode(Motor::M2, m);
}
bool DCMotorsDriver::IsError(Motor motor) {
    return bool(_GetMode(motor) & (0x1 << 6));
}
bool DCMotorsDriver::IsBusy(Motor motor) {
    return bool(_GetMode(motor) & (0x1 << 7));
}
void DCMotorsDriver::SetReverse(bool rev, Motor motor) {
    byte m = _GetMode(motor);
    if (rev)
        m |= 0x1 << 3;
    else
        m &= ~(0x1 << 3);
    _SetMode(motor, m);
}
bool DCMotorsDriver::GetReverse(Motor motor) {
    return (bool) (_GetMode(motor) & (0x1 << 3));
}
MotorMode DCMotorsDriver::GetCurrentMode(Motor motor) {
    return static_cast<MotorMode>(_GetMode(motor) & 0x3);
}
void DCMotorsDriver::SetCurrentMode(MotorMode mode, Motor motor) {
    byte m = _GetMode(motor);
    switch (mode) {
        case MotorMode::PowerCtrl:
            m &= ~0x3;
            break;
        case MotorMode::ConstSpeed:
            m &= ~0x3;
            m |= 0x01;
            break;
        case MotorMode::ToPosition:
            m &= ~0x3;
            m |= 0x02;
            break;
        case MotorMode::ResetEncoder:
            m |= 0x03;
            break;
        default:
            throw invalid_argument("Invalid motor mode is specified!");
    }
    _SetMode(motor, m);
}
sbyte DCMotorsDriver::GetPower(Motor motor) {
    switch (motor) {
        case Motor::M1:
            return ReadI2CData<sbyte>(M1Power);
        case Motor::M2:
            return ReadI2CData<sbyte>(M2Power);
        default:
            throw invalid_argument("motor");
    }
}
void DCMotorsDriver::SetPower(sbyte power, Motor motor) {
    switch (motor) {
        case Motor::M1:
            WriteI2CData(M1Power, power);
            break;
        case Motor::M2:
            WriteI2CData(M2Power, power);
            break;
        default:
            throw invalid_argument("motor");
    }
}
float DCMotorsDriver::GetBatteryVoltage() {
    uword raw = ReadI2CData<uword>(BatteryVoltage, true);
    return (float(raw >> 8) * 80.0f + float(raw & 0xFF) * 20.0f) / 1000.0f;
}
long DCMotorsDriver::GetCurrentEncoder(Motor motor) {
    switch (motor) {
        case Motor::M1:
            return ReadI2CData<long>(M1CurrentEncoder, true);
        case Motor::M2:
            return ReadI2CData<long>(M2CurrentEncoder, true);
        default:
            throw invalid_argument("motor");
    }
}
void DCMotorsDriver::Reset(Motor motor) {
    SetTargetEncoder(0, motor);
    SetCurrentMode(MotorMode::ResetEncoder, motor);
    sleep(50);
}
void DCMotorsDriver::SetTargetEncoder(long value, Motor motor) {
    switch (motor) {
        case Motor::M1:
            WriteI2CData(M1TargetEncoder, value, true);
            break;
        case Motor::M2:
            WriteI2CData(M2TargetEncoder, value, true);
            break;
        default:
            throw invalid_argument("motor");
    }
}
long DCMotorsDriver::GetTargetEncoder(Motor motor) {
    switch (motor) {
        case Motor::M1:
            return ReadI2CData<long>(M1TargetEncoder, true);
        case Motor::M2:
            return ReadI2CData<long>(M2TargetEncoder, true);
        default:
            throw invalid_argument("motor");
    }
}

DCMotor::DCMotor(DCMotorsDriver* driver, Motor motor, bool reverse)
        : _driver(driver), _motor(motor) {
    _driver->Reset(_motor);
    _driver->SetReverse(reverse, _motor);
    this->StopBrake();
}
DCMotor::~DCMotor() {
    Stop();
}
bool DCMotor::IsMotorOn() {
    switch (_driver->GetPower(_motor)) {
        case NEUTRAL_POWER:
        case 0:
            return false;
        default:
            return true;
    }
}
void DCMotor::WaitForMotor() {
    sleep(50);
    while (_driver->IsBusy(_motor))
        sleep(3);
}
void DCMotor::RideConstSpeed(sbyte speed) {
    if (speed == NEUTRAL_POWER || speed == 0)
        throw invalid_argument("invalid speed for ride!");
    //
    if (IsMotorOn()) StopBrake();
    //
    _driver->SetCurrentMode(MotorMode::ConstSpeed, _motor);
    _driver->SetPower(speed, _motor);
}
void DCMotor::RideJustPower(sbyte power) {
    if (power == NEUTRAL_POWER || power == 0)
        throw invalid_argument("invalid power for ride!");
    //if (IsMotorOn()) Stop();
    _driver->SetCurrentMode(MotorMode::PowerCtrl, _motor);
    _driver->SetPower(power, _motor);
}
void DCMotor::RidePreciseNRolls(float rolls, sbyte speed, bool wait) {
    if (speed == NEUTRAL_POWER || speed <= 0)
        throw invalid_argument("invalid speed for ride!");
    //
    if (IsMotorOn()) StopBrake();
    //
    if (rolls == 0)
        return;
    long units = static_cast<long>((float) UNITS_PER_360ROLL * rolls);
    //_driver->ResetCurrentEncoder(_motor);
    _driver->SetCurrentMode(MotorMode::ToPosition, _motor);
    _driver->SetTargetEncoder(_driver->GetCurrentEncoder(_motor) + units, _motor);
    _driver->SetPower(speed, _motor);
    if (wait) {
        WaitForMotor();
        StopBrake();
    }
}
void DCMotor::StopBrake() {
    _driver->SetPower(0, _motor);
}
void DCMotor::Stop() {
    _driver->SetPower(NEUTRAL_POWER, _motor);
}
void DCMotor::RememberPosition() {
    _driver->SetCurrentMode(MotorMode::ToPosition, _motor);
    _rem_encoder = _driver->GetCurrentEncoder(_motor);
}
float DCMotor::GetDeltaPosition() {
    _driver->SetCurrentMode(MotorMode::ToPosition, _motor);
    long cur_enc = _driver->GetCurrentEncoder(_motor);
    long delta = _rem_encoder - cur_enc;
    return float(delta) / float(UNITS_PER_360ROLL);
}
float DCMotor::GetPosition() const {
    if (_driver->GetCurrentMode(_motor) != MotorMode::ToPosition)
        throw std::runtime_error("not in servo mode!");
    return _driver->GetCurrentEncoder(_motor) * 255.f / UNITS_PER_360ROLL;
}
void DCMotor::SetPosition(float pos) {
    if (_driver->GetCurrentMode(_motor) != MotorMode::ToPosition)
        throw std::runtime_error("not in servo mode!");
    _driver->SetTargetEncoder(long(pos * UNITS_PER_360ROLL / 255.f), _motor);
}
void DCMotor::AdvancePosition(float offset) {
    if (_driver->GetCurrentMode(_motor) != MotorMode::ToPosition)
        throw std::runtime_error("not in servo mode!");
    // No range control, every value is accepted
    auto pos = GetPosition();
    SetPosition(pos + offset);
}
void DCMotor::SetTiming(byte timing) {
    if (_driver->GetCurrentMode(_motor) != MotorMode::ToPosition)
        throw std::runtime_error("not in servo mode!");
    if (timing < 0 || timing > 15) throw invalid_argument("timing");
    _driver->SetPower((sbyte) (100 / (timing + 1)), _motor);
}
void DCMotor::ServoMode() {
    _driver->Reset(_motor);
    _driver->SetCurrentMode(MotorMode::ToPosition, _motor);
    SetTiming(0);
}
void DCMotorDifChassis::WaitForMotors() {
    _leftMotor->WaitForMotor();
    _rightMotor->WaitForMotor();
}
void DCMotorDifChassis::RideConstSpeed(sbyte speed) {
    _leftMotor->RideConstSpeed(speed);
    _rightMotor->RideConstSpeed(speed);
}
void DCMotorDifChassis::RideJustPower(sbyte power) {
    _leftMotor->RideJustPower(power);
    _rightMotor->RideJustPower(power);
}
void DCMotorDifChassis::RideRolls(float rolls, sbyte speed, bool wait) {
    if (speed < 0)
        throw invalid_argument("speed < 0");
    if (speed == 0) {
        if (rolls == 0.0f) return;
        else
            throw invalid_argument("speed = 0");
    }
    if (rolls == 0)
        return;
    _leftMotor->RidePreciseNRolls(rolls, speed, false);
    _rightMotor->RidePreciseNRolls(rolls, speed, false);
    if (wait) {
        WaitForMotors();
        StopBrake();
    }
}
void DCMotorDifChassis::StopBrake() {
    _leftMotor->StopBrake();
    _rightMotor->StopBrake();
}
void DCMotorDifChassis::Stop() {
    _leftMotor->Stop();
    _rightMotor->Stop();
}
#define ROLLS_PER_TURN360 2.406f
void DCMotorDifChassis::TurnCentered(float angle, sbyte speed, bool wait) {
    float rolls = ROLLS_PER_TURN360 * angle / 360.0f;
    _leftMotor->RidePreciseNRolls(rolls, speed, false);
    _rightMotor->RidePreciseNRolls(-rolls, speed, false);
    if (wait) {
        WaitForMotors();
        StopBrake();
    }
}
void DCMotorDifChassis::TurnOnWheel(float angle, DCMotorDifChassis::Wheels center, sbyte speed, bool wait) {
    float rolls = ROLLS_PER_TURN360 * 2.0f * angle / 360.0f;
    switch (center) {
        case Left:
            _rightMotor->RidePreciseNRolls(-rolls, speed, wait);
            break;
        case Right:
            _leftMotor->RidePreciseNRolls(rolls, speed, wait);
            break;
        default:
            throw invalid_argument("center");
    }
}
void DCMotorDifChassis::RememberPositon() {
    _leftMotor->RememberPosition();
    _rightMotor->RememberPosition();
}
float DCMotorDifChassis::GetDeltaPosition() {
    float left = _leftMotor->GetDeltaPosition();
    float right = _rightMotor->GetDeltaPosition();
    return (left + right) / 2.0f;
}
}