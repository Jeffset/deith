//======================= servo.h ============================
/**************************************************************
	������ ��� ������ � ��������������
	- MyRioServoDriver
	- MyRioServo <- MyRioPreciseServo, MyRioContinuousServo
***************************************************************/

#ifndef _BEDPO_SERVO_
#define _BEDPO_SERVO_

#include "device.h"
#include "motor_interface.h"

namespace deithvenbot {
void RioControlServo(MyRioPort port, int pwmN, float angle);

///------------------------------------------------------------------------
//========================== SERVO SYSTEM =================================
///------------------------------------------------------------------------
class IServoDriver {
public:
    virtual bool IsBusy() = 0;
    
    virtual byte GetStepTime() = 0;
    virtual void SetStepTime(byte time) = 0;
    
    virtual float GetServoValue(int servo) = 0;
    virtual void SetServoValue(int servo, float pos) = 0;
    
    virtual bool Enabled() = 0;
    virtual void Enable(bool) = 0;
    virtual ~IServoDriver() {};
};

class ServoDriverPWM : public IServoDriver {
    struct pwm_regists {
        regist PWM_CNFG,
                PWM_CS,
                PWM_MAX,
                PWM_CMP;
        float value;
    };
    pwm_regists pwmRegists[3];
    regist SYSSELECT;
    int supported_pwms;
    MyRioPort _port;
public:
    bool IsBusy() override;
    
    byte GetStepTime() override;
    void SetStepTime(byte time) override;
    
    float GetServoValue(int servo) override;
    void SetServoValue(int servo, float pos) override;
    
    bool Enabled() override;
    void Enable(bool) override;
public:
    ServoDriverPWM(MyRioPort port);
    virtual ~ServoDriverPWM();
};

class ServoDriverHT : public StandardI2CDevice, public IServoDriver {
public:
    bool IsBusy() override;
    
    byte GetStepTime() override;
    void SetStepTime(byte time) override;
    
    float GetServoValue(int servo) override;
    void SetServoValue(int servo, float pos) override;
    
    bool Enabled() override;
    void Enable(bool) override;
public:
    virtual ~ServoDriverHT();
    ServoDriverHT(byte deviceChainNum, II2CSlot* slot);
};

class StandardServo {
protected:
    IServoDriver* _driver;
    int _channel;
    StandardServo(IServoDriver* driver, int channel);
    virtual ~StandardServo() {};
};

class PreciseServo : public StandardServo, public IPreciseServoMotor {
    byte _timing;
public:
    PreciseServo(IServoDriver* driver, int channel)
            : StandardServo(driver, channel) {}
    float GetPosition() const override;
    void SetPosition(float pos) override;
    void AdvancePosition(float offset) override;
    void SetTiming(byte timing) override;
};

//132.0
extern float FixDown;
extern float HighUp;
extern float LowUp;
extern float SlowestUp;
extern float SlowestDown;
extern float LowDown;
extern float HighDown;

class ContinuousServo : public StandardServo {
public:
    ContinuousServo(IServoDriver* driver, int channel);
    void StartMove(float speed);
    void Stop();
    float GetSpeed() const;
};
}
#endif
