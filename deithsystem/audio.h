#ifndef _BEDPO_AUDIO_
#define _BEDPO_AUDIO_

#include <memory>
#include <fstream>
#include "device.h"
#include "task.h"

namespace deithvenbot {

class IAudioStream {
public:
    virtual void nextSample(ushort sample[2]) =0;
    virtual bool endOfStream() const =0;
    virtual int getSampleRate() const =0;
    virtual short getChannelCount() const =0;
    virtual void resetStream()=0;
    virtual ~IAudioStream() {};
};

class WavAudio : public IAudioStream {
private:
    std::fstream in;
    int currentSample = 0;
    int _totalSamplesCount = 0;
    
    int _fileSize;
    short _channelCount;
    int _sampleRate;
    int _bytePerSecond;
    short _bytePerSample;
    short _bitPerChannel;
    int _dataSize;
    //
    std::streampos _dataStreamBeginPos;
public:
    WavAudio(const std::string& filename);
public:
    void nextSample(ushort sample[2]) final;
    virtual bool endOfStream() const;
    int getSampleRate() const final;
    short getChannelCount() const final;
    void resetStream() final;
    virtual ~WavAudio() {};
};

class SineAudio : public IAudioStream {
    float argument = 0;
    float frequency;
public:
    SineAudio(float frequency);
    void nextSample(ushort* sample) override;
    bool endOfStream() const override;
    int getSampleRate() const override;
    short getChannelCount() const override;
    void resetStream() override;
};

class MyRioAudioSystem : public SynchronizedDevice {
    bool _playing = false;
    bool running;
private:
    std::shared_ptr<IAudioStream> _currentPlaying = nullptr;
    task audio_service;
protected:
    MyRioAudioSystem();
public:
    MyRioAudioSystem(const MyRioAudioSystem&) = delete;
    MyRioAudioSystem(MyRioAudioSystem&&) = delete;
    MyRioAudioSystem& operator=(const MyRioAudioSystem&) = delete;
    MyRioAudioSystem& operator=(MyRioAudioSystem&&) = delete;
    static std::shared_ptr<MyRioAudioSystem> Instance(MyRioFpga& must_init);
    
    void play(std::shared_ptr<IAudioStream> audio);
    void resume();
    void stop();
    inline bool isPlaying() const { return _playing; }
    std::shared_ptr<IAudioStream> getAudio() const;
    virtual ~MyRioAudioSystem();
};
}
#endif
