//======================= device.h ============================
/**************************************************************
	Basic 'low-level' classes for managing
	myRIO i2c & other on-board devices
***************************************************************/
#include "util.h"
#include "task.h"
#include <memory>
#include "myriocsupport/MyRio.h"

#ifndef _BEDPO_DEVICE_
#define _BEDPO_DEVICE_
namespace deithvenbot {

class MyRioFpga { // RAII
public:
    MyRioFpga(const MyRioFpga&) = delete;
    MyRioFpga& operator=(const MyRioFpga&)= delete;
    MyRioFpga(MyRioFpga&&) = default;
    MyRioFpga& operator=(MyRioFpga&&)= default;
    MyRioFpga();
    ~MyRioFpga();
};

class SynchronizedDevice {
private:
    pthread_mutex_t _mutex;
protected:
    class locker { // RAII
        pthread_mutex_t* _mutex;
    public:
        locker(SynchronizedDevice* device);
        locker(const locker&) = delete;
        locker& operator=(const locker&)= delete;
        locker& operator=(locker&&) = delete;
        locker(locker&&);
        ~locker();
    };
    SynchronizedDevice();
    virtual ~SynchronizedDevice();
};

// MXP Expansion ports
enum class MyRioPort {
    A, B, C
};

///------------------------------------------------------------------------
//================= I2C ABSTRACTION LAYER ================================
///------------------------------------------------------------------------
class II2CSlot {
public:
    virtual byte ReadI2CSingleByte(byte _deviceChainNum)=0;
    virtual void WriteI2CSingleByte(byte _deviceChainNum, byte data)=0;
    virtual void WriteI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data, unsigned count, bool reverse)=0;
    virtual void ReadI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data, int count, bool reverse)=0;
protected:
    virtual ~II2CSlot() {}
};

class I2CPort : public SynchronizedDevice, public II2CSlot {
    static I2CPort* _A, * _B, * _C;
public:
    static I2CPort* A();
    static I2CPort* B();
    static I2CPort* C();
    static void ResetPorts();
private:
    regist I2CxADDR,
            I2CxDATO,
            I2CxDATI,
            I2CxSTAT,
            I2CxCNTL,
            I2CxGO;
private:
    I2CPort(MyRioPort letter);
    I2CPort() = delete;
public:
    byte ReadI2CSingleByte(byte _deviceChainNum) override;
    void WriteI2CSingleByte(byte _deviceChainNum, byte data) override;
    void WriteI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data, unsigned count, bool reverse) override;
    void ReadI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data, int count, bool reverse) override;
};
/**
 * @brief Базовый класс "физических" устройств, коммуникация с которыми осуществляется
 * по протоколу i2c. Представляет защищенный интерфейс для работы с i2c.
 * Подлкючается в определеннный i2c-порт в MyRIO напрямую,
 * либо использует селектор (i2c мультиплексор)
 */
class I2CDevice {
private:
    byte _deviceChainNum;
    II2CSlot* _slot;
protected:
    byte ReadI2CSingleByte();
    void WriteI2CSingleByte(byte data);
    //
    void WriteI2CAddressedBytes(byte address, byte* data, unsigned count, bool reverse = false);
    void ReadI2CAddressedBytes(byte address, byte* data, int count, bool reverse = false);
    
    template<typename D>
    void WriteI2CData(byte address, const D& data, bool bigEnd = false) {
        WriteI2CAddressedBytes(address, (byte*) &data, sizeof(data), bigEnd);
    }
    template<typename D>
    void ReadI2CData(byte address, D* data, bool bigEnd = false) {
        ReadI2CAddressedBytes(address, (byte*) data, sizeof(data), bigEnd);
    }
    template<typename D>
    D ReadI2CData(byte address, bool bigEnd = false) {
        D data;
        ReadI2CAddressedBytes(address, (byte*) &data, sizeof(data), bigEnd);
        return data;
    }
protected:
    I2CDevice(byte deviceChainNum, II2CSlot* slot);
public:
    inline II2CSlot* GetSlot() const { return _slot; }
};
///------------------------------------------------------------------------
//========================== MULTIPLEXOR SYSTEM ===========================
///------------------------------------------------------------------------
class I2CMultiplexor3 : public I2CDevice, public SynchronizedDevice {
    using session = SynchronizedDevice::locker;
    byte _active_channel;
    class MultiplexorSlot : public II2CSlot {
        I2CMultiplexor3* _gate;
        int _channel;
    public:
        MultiplexorSlot(I2CMultiplexor3* gate, int channel);
        byte ReadI2CSingleByte(byte _deviceChainNum) override;
        void WriteI2CSingleByte(byte _deviceChainNum, byte data) override;
        void WriteI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data,
                                    unsigned count, bool reverse) override;
        void ReadI2CAddressedBytes(byte _deviceChainNum, byte address, byte* data,
                                   int count, bool reverse) override;
    };
    MultiplexorSlot* slots[3];
    session OpenSession(int channel);
public:
    inline II2CSlot* NXT0() const { return slots[0]; }
    inline II2CSlot* NXT1() const { return slots[1]; }
    inline II2CSlot* NXT2() const { return slots[2]; }
    I2CMultiplexor3(II2CSlot* slot);
    ~I2CMultiplexor3();
};
///------------------------------------------------------------------------
//========================== STANDARD DEVICE ==============================
///------------------------------------------------------------------------
class StandardI2CDevice : public I2CDevice {
public:
    void GetVersion(char buff[]);
    void GetManufacturer(char buff[]);
    void GetDeviceName(char buff[]);
protected:
    StandardI2CDevice(byte deviceChainNum, II2CSlot* slot);
    virtual ~StandardI2CDevice() {};
};
///------------------------------------------------------------------------
//============================= LED SYSTEM ================================
///------------------------------------------------------------------------
// TODO Singletons for onboard devices

template<class S>
inline std::shared_ptr<S> singleton_instance(std::function<S*()>&& creator) {
    static std::weak_ptr<S> saved_instance;
    if (saved_instance.expired()) {
        auto new_instance = std::shared_ptr<S>(creator());
        saved_instance = new_instance;
        return new_instance;
    }
    else
        return saved_instance.lock();
}
///
class MyRioLEDBoard : public SynchronizedDevice {
protected:
    MyRioLEDBoard();
public:
    MyRioLEDBoard(const MyRioLEDBoard&) = delete;
    MyRioLEDBoard& operator=(const MyRioLEDBoard&)= delete;
    MyRioLEDBoard& operator=(MyRioLEDBoard&&)= delete;
    MyRioLEDBoard(MyRioLEDBoard&&) = delete;
    static std::shared_ptr<MyRioLEDBoard> Instance(MyRioFpga& must_init);
    ~MyRioLEDBoard();
    bool led(int _led);
    bool led(int _led, bool enable);
    bool toggle(int _led);
    void switchOffAll();
    void switchOnAll();
    void set(byte mask);
};

///------------------------------------------------------------------------
//======================== ACCELEROMETER SYSTEM ===========================
///------------------------------------------------------------------------
class MyRioAccelerometer {
protected:
    MyRioAccelerometer() = default;
public:
    MyRioAccelerometer(const MyRioAccelerometer&) = delete;
    MyRioAccelerometer(MyRioAccelerometer&&) = delete;
    MyRioAccelerometer& operator=(const MyRioAccelerometer&) = delete;
    MyRioAccelerometer& operator=(MyRioAccelerometer&&) = delete;
    static std::shared_ptr<MyRioAccelerometer> Instance(MyRioFpga& must_init);
    using float_t = double;
    static constexpr float_t G_FORCE = (float_t) 9.81;
    float_t get_x_accel();
    float_t get_y_accel();
    float_t get_z_accel();
};

///------------------------------------------------------------------------
//=========================== BUTTON SYSTEM ===============================
///------------------------------------------------------------------------
class MyRioButton : public SynchronizedDevice {
    std::function<void()> action = []() { logi(" <button event triggered> "); };
    NiFpga_IrqContext _irq_context;
    bool _watching;
protected:
    MyRioButton();
public:
    MyRioButton(const MyRioButton&) = delete;
    MyRioButton(MyRioButton&&) = delete;
    MyRioButton& operator=(const MyRioButton&)= delete;
    MyRioButton& operator=(MyRioButton&&)= delete;
    static std::shared_ptr<MyRioButton> Instance(MyRioFpga& must_init);
    enum Event {
        Press = 1, Release = 2, Toggle = 3
    };
    void setAction(const std::function<void()>& act, Event event);
    ~MyRioButton();
private:
    void _config();
    task _watcher;
    Event _event;
};
    
}
#endif

