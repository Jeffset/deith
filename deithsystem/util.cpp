#include <iostream>
#include <chrono>
#include "util.h"

namespace deithvenbot {

bool use_colored_logging = true;

void sleep(int ms) {
    auto sec = ms / 1000;
    auto nanosec = (ms % 1000) * 1000000;
    timespec tm{sec, nanosec};
    nanosleep(&tm, nullptr);
}
void sleep_ns(int ns) {
    timespec tm{0, ns};
    nanosleep(&tm, nullptr);
}

void logm(const char* msg) {
    std::cout << msg << std::endl;
}
void logi(const char* info) {
    if (use_colored_logging)
        std::cout << "\x1b[36m" << info << "\x1b[0m\n";
    else
        std::cout << info << std::endl;
}
void logw(const char* warn) {
    if (use_colored_logging)
        std::cout << "\x1b[33;1mWARN: " << warn << "\x1b[0m\n";
    else
        std::cout << "WARN: " << warn << std::endl;
}
void loge(const char* err) {
    if (use_colored_logging)
        std::cout << "\x1b[31;1mERROR: " << err << "\x1b[0m\n";
    else
        std::cout << "ERROR: " << err << std::endl;
}
void logok(const char* succ) {
    if (use_colored_logging)
        std::cout << "\x1b[32;1m" << succ << " -> OK\x1b[0m\n";
    else
        std::cout << succ << "-> OK\n";
}
void logok(const std::string& msg) {
    logok(msg.c_str());
}
void loge(const std::string& err) {
    loge(err.c_str());
}
void logi(const std::string& info) {
    logi(info.c_str());
}
void logm(const std::string& msg) {
    logm(msg.c_str());
}
std::string operator "" _str(const char* str, unsigned int) {
    return std::string(str);
}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Timer::_Timer_impl {
public:
    std::chrono::steady_clock::time_point _start_time;
};

Timer::Timer() {
    _impl = new _Timer_impl;
    restart();
}
void Timer::restart() {
    using namespace std::chrono;
    _impl->_start_time = steady_clock::now();
}
long Timer::get_elapsed_ms() const {
    using namespace std::chrono;
    return duration_cast<milliseconds>(steady_clock::now() - _impl->_start_time).count();
}
Timer::~Timer() {
    delete _impl;
}
}