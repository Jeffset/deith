//
// Created by marco on 05/02/17.
//

#ifndef WROMAIN_DEITHSYSTEM_H
#define WROMAIN_DEITHSYSTEM_H

#include "device.h"
#include "audio.h"
#include "motor.h"
#include "servo.h"
#include "sensor.h"

namespace deithvenbot {
/*
 * TODO: Abstract base class easy for configuring devices;
 * TODO: Auto i2c address assigning (V/X);
 * DONE: (Maybe) i2c port plugs;
 * TODO: (May be yes ^_^) Lua bindings for scripting
 */
/**
 * @brief Базовый класс модели робота, который отвечает за инициализацию
 * указанных устройств, создает связки для скриптов и текстовой оболочки
 */
class DeithvenBot {
    MyRioFpga niFpga; // RAII (must init first)

public: // Встроенные устройства контроллера MyRIO
    std::shared_ptr<MyRioButton> button = MyRioButton::Instance(niFpga);
    std::shared_ptr<MyRioAudioSystem> audio = MyRioAudioSystem::Instance(niFpga);
    std::shared_ptr<MyRioAccelerometer> accelerometer = MyRioAccelerometer::Instance(niFpga);
    std::shared_ptr<MyRioLEDBoard> ledBoard = MyRioLEDBoard::Instance(niFpga);
public:
    /**
     * @brief По умолчанию инициализируются только "onboard"-устройства MyRIO,
     * такие как акселерометр, кнопка, светодиодная панель и аудио-служба
     */
    DeithvenBot();
protected: // Фабричные методы создания и инициализации периферийных устройств
};
}

#endif //WROMAIN_DEITHSYSTEM_H
