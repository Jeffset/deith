//======================= vision.h ============================
/**************************************************************
***************************************************************/
#ifndef _BEDPO_VISION_
#define _BEDPO_VISION_

#include <vector>

namespace deithvenbot {
///------------------------------------------------------------------------
//====================== VISION SYSTEM ====================================
///------------------------------------------------------------------------

extern float AIM_TWEAK;

struct VisSpot {
    int pos, length;
    int center() const;
    int right() const {
        return pos + length;
    }
    VisSpot(int pos, int length) : pos(pos), length(length) {}
};

struct VisRectangle {
    int X, Y, Width, Height;
    VisRectangle(int X, int Y, int Width, int Height)
            : X(X), Y(Y), Width(Width), Height(Height) {}
    VisRectangle() = default;
};

using VisSpots = std::vector<VisSpot>;

class VisionSystem {
    class _OpenCvImpl;
    _OpenCvImpl* _impl;
    VisRectangle region;
    int _cached_upper_line;
    int _vert_shift;
public:
    int getVertShift() const { return _vert_shift; }
    int getCachedUpperLine() const { return _cached_upper_line; };
    VisionSystem();
    ~VisionSystem();
    VisionSystem* Init();
    VisionSystem* See();
    VisionSystem* See(int frame_skip);
    VisionSystem* ThinkOfWhite(const VisRectangle& region);
    VisionSystem* ThinkOfGreen(const VisRectangle& region);
    VisSpots ExtractSpots(int line, bool draw, bool unite = true);
    void UniteSpots(VisSpots& spots);
    
    bool FindLines(int& upper_line, int& lower_line);
    void CalcShift();
    
    void RemoveGreen(VisSpots& spots, VisSpots& green_spots);
    VisionSystem* Save(const char* fileName);
    VisionSystem* Save(const std::string& fileName);
    float CalculateAimFactor(int center);
    int GetMaxGap(const VisSpots& spots);
};
}
#endif // _BEDPO_VISION_
