#include "../deithsystem/util.h"
#include "bowling_vision.h"

#include<opencv2/opencv.hpp>

namespace deithvenbot {

#define FRAME_WIDTH   640
#define FRAME_HEIGHT  480
float AIM_TWEAK = 0.5392f;
#define PINS_CHECK_HEIGHT_MAX 30
#define PINS_CHECK_HEIGHT_MIN 5


class VisionSystem::_OpenCvImpl {
public:
    cv::VideoCapture camera;
    cv::Mat tex;
    cv::Mat roi;
    std::vector<int> flags;
    
    _OpenCvImpl()
            : camera(CV_CAP_ANY) {
        flags.push_back(cv::IMWRITE_JPEG_QUALITY);
        flags.push_back(80);
    }
    
    virtual ~_OpenCvImpl() {
        tex.release();
        roi.release();
        camera.release();
    }
};

int VisSpot::center() const {
    return pos + length / 2;
}

VisionSystem::VisionSystem() {
    _impl = new _OpenCvImpl();
    if (!_impl->camera.isOpened()) {
        throw std::runtime_error("Can't init camera capture!");
    }
    _impl->camera.set(CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
    _impl->camera.set(CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
}
VisionSystem* VisionSystem::Init() {
    cv::Mat tmp;
    for (int i = 0; i < 10; i++)
        _impl->camera >> tmp;
    return this;
}
VisionSystem* VisionSystem::See() {
    return See(10);
}
VisionSystem* VisionSystem::See(int frame_skip) {
    using namespace cv;
    if (frame_skip <= 0)
        throw std::invalid_argument("frame_skip <= 0");
    for (int i = 0; i < frame_skip; i++) {
        _impl->camera >> _impl->tex;
        sleep(8);
    }
    return this;
}
VisionSystem* VisionSystem::ThinkOfWhite(const VisRectangle& regn) {
    using namespace cv;
    this->region = regn;
    extractChannel(_impl->tex, _impl->tex, 1);
    cv::Rect roi_rect(region.X, region.Y, region.Width, region.Height);
    _impl->roi = _impl->tex(roi_rect);
    //blur(_impl->roi, _impl->roi, cv::Size(10, 15));
    threshold(_impl->roi, _impl->roi, 0.0, 255.0, cv::THRESH_OTSU);
    return this;
    
}
VisionSystem* VisionSystem::ThinkOfGreen(const VisRectangle& regn) {
    using namespace cv;
    this->region = regn;
    cv::Rect roi_rect(region.X, region.Y, region.Width, region.Height);
    _impl->roi = _impl->tex(roi_rect);
    cv::Mat hue, lightness, saturation;
    auto texture = _impl->roi;
    cv::blur(texture, texture, {5, 5});
    
    cv::cvtColor(texture, texture, CV_BGR2HLS_FULL);
    cv::extractChannel(texture, hue, 0);
    cv::extractChannel(texture, lightness, 1);
    cv::extractChannel(texture, saturation, 2);
    
    cv::inRange(hue, cv::Scalar{60}, cv::Scalar{140}, hue);
    cv::inRange(lightness, cv::Scalar{3}, cv::Scalar{100}, lightness);
    cv::inRange(saturation, cv::Scalar{40}, cv::Scalar{255}, saturation);
    cv::bitwise_and(hue, lightness, hue);
    cv::bitwise_and(hue, saturation, hue);
    _impl->roi = hue;
    _impl->tex = hue;
    return this;
}

VisSpots VisionSystem::ExtractSpots(int line, bool draw, bool unite) {
    using namespace std;
    using namespace cv;
    int y = line;
    VisSpots spots;
    bool is_spot = false;
    int pos = 0;
    int length = 0;
    for (int x = 0; x < region.Width; x++) {
        byte color = _impl->roi.at<byte>(y, x);
        if (color > 50) {
            if (!is_spot) {
                pos = x;
                is_spot = true;
            }
            length++;
        }
        else {
            if (is_spot) {
                spots.push_back({pos, length});
                is_spot = false;
                length = 0;
            }
        }
    }
    if (is_spot)
        spots.push_back({pos, length});
    if (unite)
        UniteSpots(spots);
    if (draw) {
        cv::line(_impl->roi, Point(0, y), Point(region.Width, y), Scalar(0.5));
        for (auto spot : spots) {
            auto center = spot.center();
            circle(_impl->roi, Point(center, y), 2, Scalar(0.5f));
        }
    }
    return spots;
    /*#define MAX_UNITE_GAP 14
    int y = line;
    VisSpots ans;
    int pointer = 0;
    for (int x = 0; x < region.Width; ++x) {
        if (_impl->roi.at<byte>(y, x) > 128) {
            VisSpot detected{x, 1};
            bool has_mistake = false;
            pointer = x;
            while (!has_mistake) {
                for (int x2 = pointer + 1; _impl->roi.at<byte>(y, x2) > 128; ++x2) {
                    detected.length++;
                    pointer = x2;
                }
                has_mistake = true;
                for (int x2 = pointer + 1; x2 < pointer + MAX_UNITE_GAP + 1; ++x2) {
                    if (_impl->roi.at<byte>(y, x2) > 128) {
                        has_mistake = false;
                        detected.length += x2 - pointer;
                        pointer = x2;
                        break;
                    }
                }
            }
            x = pointer + 1;
            ans.push_back(detected);
        }
    }
    if (draw) {
        cv::line(_impl->roi, Point(0, y), Point(region.Width, y), Scalar(0.5));
        for (auto& spot : ans) {
            auto center = spot.center();
            circle(_impl->roi, Point(center, y), 2, Scalar(0.5f));
        }
    }
    return ans;*/
}
float VisionSystem::CalculateAimFactor(int center) {
    auto bias = center + region.X - float(FRAME_WIDTH) * AIM_TWEAK;
    return bias / float(FRAME_WIDTH) * 2.f;
}
VisionSystem* VisionSystem::Save(const char* fileName) {
    //logm("saved to "_str + fileName);
    int x = (int) (FRAME_WIDTH * AIM_TWEAK);
    line(_impl->tex, cv::Point(x, 0), cv::Point(x, FRAME_HEIGHT), cv::Scalar(1.0, 1.0));
    imwrite(fileName, _impl->tex, _impl->flags);
    return this;
}
VisionSystem::~VisionSystem() {
    if (_impl) delete _impl;
}
bool VisionSystem::FindLines(int& upper_line, int& lower_line) {
    int upper = 0;
    int y = 0;
    do {
        y++;
        auto spots = ExtractSpots(y, false);
        if (spots.size() > 0) {
            upper = y;
            break;
        }
    } while (y < region.Height);
    if (y >= region.Height)
        return false;
    upper_line = upper + 7;
    lower_line = upper + 20;
    _cached_upper_line = upper_line;
    return true;
}
VisionSystem* VisionSystem::Save(const std::string& fileName) {
    Save(fileName.c_str());
    return this;
}
void VisionSystem::UniteSpots(VisSpots& spots) {
    VisSpots new_spots;
    if (spots.empty())
        return;
    bool last_united = false;
    for (int i = 0; i < (int)spots.size() - 1; ++i) {
        if ((spots[i + 1].pos - spots[i].right()) < 14) {
            int left = spots[i].pos;
            int right = spots[i + 1].right();
            for (; i < (int)spots.size() - 1; ++i) {
                if ((spots[i + 1].pos - spots[i].right()) < 14) {
                    right = spots[i + 1].right();
                }
                else break;
            }
            new_spots.push_back({left, right - left});
            if (i + 1 == (int)spots.size())
                last_united = true;
        }
        else {
            new_spots.push_back(spots[i]);
        }
    }
    if (!last_united)
        new_spots.push_back(spots.back());
    spots = new_spots;
}
void VisionSystem::RemoveGreen(VisSpots& spots, VisSpots& green_spots) {
    /*for (auto iter = spots.begin(); iter != spots.end(); ++iter) {
        if (iter->length <= 10) {
            spots.erase(iter);
            break;
        }
    }*/
    m_begin:
    if (green_spots.empty() || spots.empty())
        return;
    for (auto iter = spots.begin(); iter != spots.end(); ++iter) {
        for (auto iter_gr = green_spots.begin(); iter_gr != green_spots.end(); ++iter_gr) {
            if (abs(iter->center() - iter_gr->center()) <= 5) {
                spots.erase(iter);
                logm("green intersection!");
                goto m_begin;
            }
        }
    }
}
void VisionSystem::CalcShift() {
    int y = 0;
    do {
        y++;
        auto spots = ExtractSpots(y, false, false);
        if (spots.size() == 1 && spots.front().length >= (region.Width * 0.75f))
            break;
    } while (y < region.Height);
    _vert_shift = y;
    logi("shift = "_str + std::to_string(y));
    if (_vert_shift == region.Height)
        logw("possibly error in shift calculation!");
}
int VisionSystem::GetMaxGap(const VisSpots& spots) {
    if (spots.empty())
        return 0;
    int max_gap = 0;
    for (int i = 0; i < (int)spots.size() - 1; ++i) {
        auto gap = spots[i + 1].pos - spots[i].right();
        max_gap = gap > max_gap ? gap : max_gap;
    }
    return max_gap;
}
    
}



























