
#ifndef MYRIO_COMMANDS_H
#define MYRIO_COMMANDS_H

#include "bedpo.h"
#include "shell.h"

#define COMM_ARGS BedpoSystem* sys, std::vector<std::string>& args
#define EXIT_REQ 228

namespace deithvenbot {
namespace wro2016 {

void cmd_exit(COMM_ARGS);
void cmd_restart(COMM_ARGS);
void cmd_blink(COMM_ARGS);
void cmd_color(COMM_ARGS);
void cmd_ride(COMM_ARGS);
void cmd_mstop(COMM_ARGS);
void cmd_launch(COMM_ARGS);
void cmd_turn(COMM_ARGS);
void cmd_analog(COMM_ARGS);
void cmd_ev3color(COMM_ARGS);
void cmd_cservo(COMM_ARGS);
void cmd_pservo(COMM_ARGS);
void cmd_sleep(COMM_ARGS);
void cmd_reload_set(COMM_ARGS);
void cmd_accel(COMM_ARGS);
void cmd_centrate(COMM_ARGS);
void cmd_see_left(COMM_ARGS);
void cmd_see_green(COMM_ARGS);
void cmd_see_right(COMM_ARGS);
void cmd_fixhand(COMM_ARGS);
void cmd_audio(COMM_ARGS);
void cmd_manipulator(COMM_ARGS);
}
}

#endif //MYRIO_COMMANDS_H
