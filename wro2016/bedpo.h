//======================= bedpo.h ============================
/**************************************************************
	All subsystems are united in BEDPO,
	Main library header file
***************************************************************/
#ifndef _BEDPO_BEDPO_
#define _BEDPO_BEDPO_

#include "../deithsystem/device.h"
#include "../deithsystem/audio.h"
#include "../deithsystem/servo.h"
#include "../deithsystem/motor.h"
#include "../deithsystem/sensor.h"
#include "bowling_vision.h"

namespace deithvenbot {
namespace wro2016 {
///------------------------------------------------------------------------
//================== OVERALL BEDPO ROBOT SYSTEM ===========================
///------------------------------------------------------------------------
class BedpoSystem {
    MyRioFpga my_rio;
    //
    void printInitInfo(StandardI2CDevice* device);
    void printSystemStat(const char* sys_name, const char* error);
    // NI MYRIO FPGA
    // DRIVERS
    ServoDriverHT* ServoDriver;
    DCMotorsDriver* MotorsDriver;
    I2CMultiplexor3* Multiplexor;

public:
    // ONBOARD Devices:
    std::shared_ptr<MyRioButton> Button = MyRioButton::Instance(my_rio);
    std::shared_ptr<MyRioAudioSystem> AudioSystem = MyRioAudioSystem::Instance(my_rio);
    std::shared_ptr<MyRioLEDBoard> LedBoard = MyRioLEDBoard::Instance(my_rio);
    std::shared_ptr<MyRioAccelerometer> Accelerometer = MyRioAccelerometer::Instance(my_rio);
    
    // VISION
    VisionSystem* Vision;
    // DEVICE OBJECTS
    PreciseServo* TowerServo = nullptr;
    ContinuousServo* HandServo = nullptr;
    HiTechColorSensor* SensorHand = nullptr;
    HiTechColorSensor* SensorRack = nullptr;
    EV3ColorSensor* RightSensor = nullptr;
    DCMotor* LeftMotor = nullptr;
    DCMotor* RightMotor = nullptr;
    DCMotorDifChassis* Motors = nullptr;
    AnalogSensor* LeftSensor = nullptr;
public:
    // STATUS GETTERS
    inline bool IsServoSysInit() const { return (bool) ServoDriver; }
    inline bool IsMotorSysInit() const { return bool(MotorsDriver); }
    inline bool IsVisionSysInit() const { return bool(Vision); }
    inline bool IsHandSensorSysInit() const { return bool(SensorHand); }
    inline bool IsRackSensorSysInit() const { return bool(SensorRack); }
    inline bool IsLeftSensorSysInit() const { return bool(LeftSensor); }
    inline bool IsRightSensorSysInit() const { return bool(RightSensor); }
    inline bool AreAllSysInit() const {
        return
                IsServoSysInit() &&
                IsVisionSysInit() &&
                IsMotorSysInit() &&
                IsHandSensorSysInit() &&
                IsLeftSensorSysInit() &&
                IsRightSensorSysInit() &&
                IsRackSensorSysInit();
    };
public:
    enum LedIndicatorShow {
        WaveSingle, AllBlink, PingPong
    };
    void Indicate(LedIndicatorShow indication);
private:
    void InitSensors();
    void InitServos();
    void InitMotors();
    void InitVision();
public:
    BedpoSystem();
    virtual ~BedpoSystem();
};
}
}
#endif