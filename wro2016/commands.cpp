#include "wro2016_class.h"
#include "commands.h"

#include <sstream>
#include <algorithm>
#include <iomanip>

using namespace std;

#include "../cursed-ui/cursed_ui.h"

namespace deithvenbot {
namespace wro2016 {

void cmd_exit(BedpoSystem* sys, std::vector<std::string>& args) {
    throw EXIT_REQ;
}
void cmd_blink(BedpoSystem* sys, std::vector<std::string>& args) {
    if (args.size() == 0)
        sys->Indicate(BedpoSystem::WaveSingle);
    else {
        if (args[0] == "wave")
            sys->Indicate(BedpoSystem::WaveSingle);
        else if (args[0] == "all")
            sys->Indicate(BedpoSystem::AllBlink);
        else
            throw invalid_argument(args[0]);
    }
}
void cmd_color(BedpoSystem* sys, std::vector<std::string>& args) {
    bool changes_mode = false, show_rgb = false;
    bool wait_mode = false;
    ColorEnum wait_for = (ColorEnum) -1;
    int ms = 0;
    auto iter = find(args.begin(), args.end(), "-changes");
    if (iter != args.end()) {
        args.erase(iter);
        if (iter == args.end())
            throw invalid_argument("time in ms expected!");
        istringstream iss(*iter);
        iss >> ms;
        if (ms <= 0)
            throw invalid_argument(*iter);
        args.erase(iter);
        changes_mode = true;
    }
    iter = find(args.begin(), args.end(), "-rgb");
    if (iter != args.end()) {
        show_rgb = true;
        args.erase(iter);
    }
    iter = find(args.begin(), args.end(), "-wait");
    if (iter != args.end()) {
        if (changes_mode)
            throw invalid_argument("mode flags conflict");
        args.erase(iter);
        if (iter == args.end())
            throw invalid_argument("color expected!");
        wait_for = get_color_value_by_name(*iter);
        wait_mode = true;
        args.erase(iter);
    }
    
    AbstractColorSensor* sensor;
    if (args.size() == 0) {
        if (!sys->IsHandSensorSysInit())throw unmet_requirement("Hand Sensor");
        sensor = sys->SensorHand;
    }
    else {
        if (args[0] == "hand") {
            if (!sys->IsHandSensorSysInit())throw unmet_requirement("Hand Sensor");
            sensor = sys->SensorHand;
        }
        else if (args[0] == "rack") {
            if (!sys->IsRackSensorSysInit())throw unmet_requirement("Rack Sensor");
            sensor = sys->SensorRack;
        }
        else if (args[0] == "analog") {
            if (!sys->RightSensor) throw unmet_requirement("Light Sensor");
            sensor = sys->RightSensor;
        }
        else if (args[0] == "ev3") {
            if (!sys->LeftSensor) throw unmet_requirement("Ev3 Color Sensor");
            sensor = sys->LeftSensor;
        }
        else
            throw invalid_argument(args[0]);
    }
    
    if (changes_mode) {
        logi("color changes mode:");
        ColorEnum color = (ColorEnum) -1;
        Timer timer;
        do {
            auto current = sensor->GetEnumedColor();
            if (show_rgb) {
                cout << '\r';
                cout.flush();
                cout << setw(20);
                auto rgbSensor = dynamic_cast<HiTechColorSensor*>(sensor);
                if (rgbSensor == nullptr)
                    throw invalid_argument("this _sensor does not support rgb colors!");
                cout << get_color_name(current) << ' ' << rgbSensor->GetRGB().getStr();
                cout.flush();
                color = current;
            }
            else if (current != color) {
                cout << '\r';
                cout.flush();
                cout << setw(15);
                cout << get_color_name(current);
                cout.flush();
                color = current;
            }
            sleep(30);
        } while (timer.get_elapsed_ms() < ms);
        cout << endl;
    }
    else if (wait_mode) {
        logi("Begin waiting (20 sec timeout) ...");
        auto found = sensor->Wait(UntilColor, wait_for, 20000);
        if (found == wait_for)
            logok("color was found.");
        else
            logi("timeout exceed.");
    }
    else {
        cout << sensor->GetEnumedColorName();
        auto rgbSensor = dynamic_cast<HiTechColorSensor*>(sensor);
        if (show_rgb && rgbSensor)
            cout << ' ' << rgbSensor->GetRGB().getStr();
        cout << endl;
    }
}
void cmd_restart(BedpoSystem* sys, vector<string>& args) {
    throw RESTART_REQ;
}
void cmd_launch(BedpoSystem* sys, vector<string>& args) {
    if (!sys->AreAllSysInit()) {
        sys->Indicate(BedpoSystem::AllBlink);
        throw unmet_requirement("All systems");
    }
    auto wro_bedpobot = dynamic_cast<WROBowlingChallenge*>(sys);
    wro_bedpobot->mainProgram();
}
void cmd_ride(BedpoSystem* sys, vector<string>& args) {
    if (!sys->IsMotorSysInit())
        throw unmet_requirement("Motors");
    if (args.size() == 0)
        throw invalid_argument("[float] 'rolls' required");
    float rolls = 0.0f;
    int speed = 20;
    bool wait = true;
    auto iter = find(args.begin(), args.end(), "-nowait");
    if (iter != args.end()) {
        wait = false;
        args.erase(iter);
    }
    istringstream iss(args[0]);
    iss >> rolls;
    auto abs_rolls = rolls > 0.0f ? rolls : -rolls;
    if (abs_rolls > 10.0f) {
        logw("strangely too big number of rolls!");
        return;
    }
    if (args.size() >= 2) {
        iss.str(args[1]);
        iss >> speed;
        if (speed < 0) {
            loge("speed is less zero!");
            return;
        }
        else if (speed > 100) {
            logw("speed is greater 100, using 100");
            speed = 100;
        }
    }
    logm("Riding...");
    sys->Motors->RideRolls(rolls, (sbyte) speed, wait);
    logm("Finished.");
}
void cmd_turn(BedpoSystem* sys, vector<string>& args) {
    if (!sys->IsMotorSysInit())
        throw unmet_requirement("Motors");
    
    float degrees = 0.0f;
    int speed = 20;
    bool centered = true;
    DCMotorDifChassis::Wheels wheel = (DCMotorDifChassis::Wheels) -1;
    
    auto iter = find(args.begin(), args.end(), "-w");
    if (iter != args.end()) {
        args.erase(iter); // TODO test this
        if (iter == args.end())
            throw invalid_argument("wheel [left|right] expected!");
        if (*iter == "left")
            wheel = DCMotorDifChassis::Left;
        else if (*iter == "right")
            wheel = DCMotorDifChassis::Right;
        else
            throw invalid_argument("invalid wheel specified!");
        args.erase(iter);
        centered = false;
    }
    
    if (args.size() == 0)
        throw invalid_argument("[float] 'degrees' required");
    
    istringstream iss(args[0]);
    iss >> degrees;
    if (args.size() >= 2) {
        iss.str(args[1]);
        iss >> speed;
        if (speed < 0) {
            loge("speed is less zero!");
            return;
        }
        else if (speed > 100) {
            logw("speed is greater 100, using 100");
            speed = 100;
        }
    }
    
    logm("Turning...");
    if (centered)
        sys->Motors->TurnCentered(degrees, (sbyte) speed, true);
    else
        sys->Motors->TurnOnWheel(degrees, wheel, (sbyte) speed, true);
    logm("Finished.");
}
void cmd_analog(BedpoSystem* sys, vector<string>& args) {
    bool cycle = false;
    bool raw = false;
    char mode = '-'; // measure
    int ms = 0;
    auto iter = find(args.begin(), args.end(), "-calib");
    if (iter != args.end()) {
        args.erase(iter);
        if (iter == args.end())
            mode = 'c';
        else if (*iter == "zero")
            mode = '0';
        else if (*iter == "one")
            mode = '1';
        else
            throw invalid_argument(*iter);
        args.erase(iter);
    }
    iter = find(args.begin(), args.end(), "-raw");
    if (iter != args.end()) {
        args.erase(iter);
        if (mode != '-')
            throw invalid_argument("invalid flags combo!");
        raw = true;
    }
    iter = find(args.begin(), args.end(), "-loop");
    if (iter != args.end()) {
        args.erase(iter);
        if (mode != '-')
            throw invalid_argument("can't loop in calibration");
        if (iter == args.end())
            throw invalid_argument("time duration expected!");
        istringstream iss(*iter);
        iss >> ms;
        if (ms <= 0)
            throw invalid_argument(*iter);
        cycle = true;
    }
    switch (mode) {
        case '-': {
            if (cycle) {
                Timer timer;
                do {
                    auto val = raw ? sys->LeftSensor->RetrieveRawValue() : sys->LeftSensor->GetLightness();
                    cout << setw(10);
                    cout << val;
                    cout.flush();
                    sleep(20);
                    cout << '\r';
                } while (timer.get_elapsed_ms() < ms);
                cout << endl;
            }
            else {
                auto val = raw ? sys->LeftSensor->RetrieveRawValue() : sys->LeftSensor->GetLightness();
                cout << val << endl;
            }
            break;
        }
        case 'c':
            logi("Point _sensor at black to calibrate zero (and press Enter):");
            getchar();
            sys->LeftSensor->CalibrateZero();
            cout << "zero = " << sys->LeftSensor->getZero() << endl;
            logi("Point _sensor at white to calibrate one (and press Enter):");
            getchar();
            sys->LeftSensor->CalibrateZero();
            cout << "one = " << sys->LeftSensor->getOne() << endl;
            logi("Calibration finished.");
            break;
        case '0':
            sys->LeftSensor->CalibrateZero();
            cout << "zero = " << sys->LeftSensor->getZero() << endl;
            break;
        case '1':
            sys->LeftSensor->CalibrateOne();
            cout << "one = " << sys->LeftSensor->getOne() << endl;
            break;
    }
}
void cmd_ev3color(BedpoSystem* sys, vector<string>& args) {
    if (!sys->RightSensor)
        throw unmet_requirement("LeftSensor");
    if ((args.size() == 1 && args[0] == "color") || args.size() == 0) {
        cout << sys->RightSensor->GetEnumedColorName() << endl;
    }
    else if (args.size() == 1) {
        if (args[0] == "refl") {
            cout << sys->RightSensor->GetLightness() << endl;
        }
        else if (args[0] == "ambi") {
            cout << sys->RightSensor->getAmbiance() << endl;
        }
        else
            throw invalid_argument(args[0]);
    }
    else
        throw invalid_argument("invalid arg(s)");
}
void cmd_cservo(BedpoSystem* sys, vector<string>& args) {
    if (!sys->IsServoSysInit())
        throw unmet_requirement("StandardServo");
    ContinuousServo* servo = sys->HandServo;
    float value;
    if (args.size() != 1)
        throw invalid_argument("argument(s) expected!");
    istringstream iss(args[0]);
    iss >> value;
    if (iss.fail()) {
        if (args[0] == "Stop") {
            servo->Stop();
            return;
        }
        else
            throw invalid_argument(args[0]);
    }
    servo->StartMove(value);
}
void cmd_pservo(BedpoSystem* sys, vector<string>& args) {
    if (!sys->IsServoSysInit())
        throw unmet_requirement("StandardServo");
    PreciseServo* servo = sys->TowerServo;
    float value;
    if (args.size() != 1)
        throw invalid_argument("argument(s) expected!");
    istringstream iss(args[0]);
    iss >> value;
    if (iss.fail()) {
        throw invalid_argument(args[0]);
    }
    servo->SetPosition(value);
}
void cmd_sleep(BedpoSystem* sys, vector<string>& args) {
    if (args.size() != 1)
        throw invalid_argument("invalid args");
    istringstream iss(args[0]);
    int ms;
    iss >> ms;
    if (iss.fail() || ms <= 0)
        throw invalid_argument(args[0]);
    sleep(ms);
}
void cmd_mstop(BedpoSystem* sys, vector<string>& args) {
    if (!sys->IsMotorSysInit())
        throw unmet_requirement("Motors");
    bool soft = false;
    if (args.size() == 1) {
        if (args.front() == "-soft")
            soft = true;
        else
            throw invalid_argument(args.front());
    }
    else if (args.size() > 1)
        throw invalid_argument("invalid args");
    if (soft)
        sys->Motors->Stop();
    else
        sys->Motors->StopBrake();
}
void cmd_reload_set(BedpoSystem* sys, vector<string>& args) {
    auto wrobot = dynamic_cast<WROBowlingChallenge*>(sys);
    wrobot->loadSettings();
}
void cmd_accel(BedpoSystem* sys, vector<string>& args) {
    Timer timer;
    auto& accel = sys->Accelerometer;
    while (timer.get_elapsed_ms() < 20000) {
        cout << setprecision(4);
        cout << setw(10) << accel->get_x_accel() << ", " <<
             setw(10) << accel->get_y_accel() << ", " <<
             setw(10) << accel->get_z_accel();
        cout.flush();
        sleep(10);
        cout.put('\r');
    }
    cout.put('\n');
}
void cmd_centrate(BedpoSystem* sys, vector<string>& args) {
    auto wrosys = (WROBowlingChallenge*) sys;
    wrosys->centrateBySensors(false);
}
void cmd_see_left(BedpoSystem* sys, vector<string>& args) {
    auto wrosys = (WROBowlingChallenge*) sys;
    wrosys->firstLook();
}
void cmd_see_right(BedpoSystem* sys, vector<string>& args) {
    auto wrosys = (WROBowlingChallenge*) sys;
    wrosys->secondLook();
}
void cmd_see_green(BedpoSystem* sys, vector<string>& args) {
    if (!sys->IsVisionSysInit())
        throw unmet_requirement("Vision");
    auto wro_sys = dynamic_cast<WROBowlingChallenge*>(sys);
    auto Vision = wro_sys->Vision;
    Vision->See()->ThinkOfGreen(wro_sys->leftInvestRegion());
    int u_line, l_line;
    Vision->FindLines(u_line, l_line);
    auto u_spots = Vision->ExtractSpots(u_line, false);
    auto l_spots = Vision->ExtractSpots(l_line, false);
    if (l_spots.size() == 0) {
        loge("no spots detected!");
        return;
    }
    int point = l_spots.front().pos;
    cout << "most left lower point = " << point << endl;
    bool right = false;
    if (point <= 75) {
        logi("left");
    }
    else if (point > 75 && point <= 125) {
        logi("center");
    }
    else if (point > 125 && point <= 180) {
        right = true;
        logi("right");
    }
    else {
        loge("something too right");
    }
    int length = l_spots.back().right() - l_spots.front().pos;
    cout << "lower length = " << length << endl;
    if (right) {
        if (length > 85)
            logok("obstacle detected!");
        else
            logi("simple");
    }
    else
        logi("task is unknown");
    Vision->Save("debug_green.jpg");
}
void cmd_fixhand(BedpoSystem* sys, vector<string>& args) {
    { // block, where fixing is enabled
        HandFixator fixator(sys->SensorHand, sys->HandServo);
        sleep(20_s);
    } // fixing disabled
}
void cmd_audio(BedpoSystem* sys, vector<string>& args) {
    if (args.size() < 1)
        throw invalid_argument("audio command expected!");
    if (args[0] == "resume") {
        if (args.size() < 2)
            sys->AudioSystem->resume();
        else {
            auto wavAudio = std::make_shared<WavAudio>("./sound/"_str + args[1] + ".wav");
            sys->AudioSystem->play(wavAudio);
        }
    }
    else if (args[0] == "stop") {
        sys->AudioSystem->stop();
    }
    else if (args[0] == "f") {
        if (args.size() < 2)
            throw invalid_argument("frequency expected!");
        istringstream iss(args[1]);
        float freq;
        iss >> freq;
        auto sineAudio = std::make_shared<SineAudio>(freq);
        sys->AudioSystem->play(sineAudio);
    }
    else
        throw invalid_argument("command");
}

void cmd_manipulator(BedpoSystem* sys, vector<string>& args) {
    if (!sys->IsMotorSysInit())
        throw unmet_requirement("Motor System");
    auto M2 = sys->RightMotor;
    auto M1 = sys->LeftMotor;
    logm("start all");
    M2->ServoMode();
    M1->ServoMode();
    M2->SetTiming(3);
    M1->SetTiming(3);
    logm("mode set");
    for (int i = 0; i < 2; ++i) {
        M2->SetPosition(-70);
        M1->SetPosition(+90);
        M1->WaitForMotor();
        //
        M2->SetPosition(+70);
        M1->SetPosition(-90);
        M1->WaitForMotor();
        
        M2->SetPosition(+90);
        M1->SetPosition(+90);
        M1->WaitForMotor();
        
        M2->SetPosition(-90);
        M1->SetPosition(-90);
        M1->WaitForMotor();
        
        M2->SetPosition(0);
        M1->SetPosition(0);
        M1->WaitForMotor();
    }
    //
    M2->StopBrake();
    M1->StopBrake();
}
}
}




















