#include "bedpo.h"
#include <sstream>
#include <iostream>

#define AND
extern NiFpga_Session myrio_session;

namespace deithvenbot {
namespace wro2016 {
// CONFIGURATION
#define SERVO_I2C_ADDRESS 0x1
#define MOTOR_I2C_ADDRESS 0x2
#define CONTROL_PORT MyRioPort::B
#define SENSOR_PORT MyRioPort::A
#define TOWER_SERVO_CHANNEL 1
#define HAND_SERVO_CHANNEL 2
#define SENSOR_HAND_CHANNEL 0
#define SENSOR_RACK_CHANNEL 1
#define SENSOR_ANALOG_CHANNEL 2
//
using namespace std;

void BedpoSystem::printInitInfo(StandardI2CDevice* device) {
    ///
    char buffer[20];
    device->GetDeviceName(buffer);
    ostringstream oss;
    oss << buffer;
    device->GetVersion(buffer);
    oss << " [" << buffer << "] ";
    device->GetManufacturer(buffer);
    oss << "by \'" << buffer << "\'";
    logok(oss.str());
}
void BedpoSystem::printSystemStat(const char* sys_name, const char* error) {
    ostringstream oss;
    oss << sys_name << " is ";
    if (error) {
        oss << "not ready: " << error;
        loge(oss.str());
    }
    else {
        oss << "OK";
        logok(oss.str());
    }
}

BedpoSystem::BedpoSystem() {
    try {
        Button->setAction([ this ]() {
            if (AudioSystem->isPlaying())
                AudioSystem->stop();
            else
                AudioSystem->resume();
        }, MyRioButton::Release);
        LedBoard->led(0, true);
        logi("Initializing Devices...");
        InitSensors();
        LedBoard->led(1, true);
        InitServos();
        InitMotors();
        LedBoard->led(2, true);
        InitVision();
        LedBoard->led(3, true);
        if (AreAllSysInit())
            logi("Initialization OK.");
        else
            logw("Some subsystems failed to initialize.");
    }
    catch (const std::exception& e) {
        loge("Fatal error! Initialization failed! Cause:");
        loge(e.what());
        loge("Finalizing myRIO...");
        sleep(100);
        LedBoard->switchOffAll();
        throw e;
    }
    LedBoard->switchOffAll();
}
void BedpoSystem::InitVision() {
    try {
        logi("Initializing Vision...");
        Vision = new VisionSystem();
        Vision->Init();
        printSystemStat("Vision", nullptr);
    } catch (runtime_error& e) {
        Vision = nullptr;
        printSystemStat("Vision", e.what());
    }
}
void BedpoSystem::InitMotors() {
    try {
        logi("Initializing Motors...");
        MotorsDriver = new DCMotorsDriver(MOTOR_I2C_ADDRESS, I2CPort::B());
        printInitInfo(MotorsDriver);
        // Display system battery voltage
        cout << "Battery voltage: " << MotorsDriver->GetBatteryVoltage() << " V.\n";
        LeftMotor = new DCMotor(MotorsDriver, Motor::M1, true);
        RightMotor = new DCMotor(MotorsDriver, Motor::M2, false);
        Motors = new DCMotorDifChassis(LeftMotor, RightMotor);
        printSystemStat("Motor", nullptr);
    } catch (runtime_error& e) {
        MotorsDriver = nullptr;
        RightMotor = nullptr;
        LeftMotor = nullptr;
        Motors = nullptr;
        printSystemStat("Motor", e.what());
    }
}
void BedpoSystem::InitSensors() {
    try {
        logi("Initializing Sensors...");
        logm("Analog light sensor:");
        LeftSensor = new AnalogSensor(SENSOR_ANALOG_CHANNEL, SENSOR_PORT);
        auto ok = LeftSensor->Test();
        if (ok != 0) {
            LeftSensor = nullptr;
            if (ok < 0)
                loge("No Analog Sensor: analog value too small!");
            else
                loge("No Analog Sensor: analog value too high!");
        }
        else
            logok("Analog Sensor is detected");
        Multiplexor = new I2CMultiplexor3(I2CPort::A());
        logm("Ev3 adapter for color sensor:");
        try {
            RightSensor = new EV3ColorSensor(I2CPort::B());
            printInitInfo(RightSensor);
        } catch (runtime_error& e) {
            logw("No Ev3 adapter for color sensor!");
            RightSensor = nullptr;
        }
        
        logm("Hand sensor:");
        try {
            SensorHand = new HiTechColorSensor(Multiplexor->NXT0());
            printInitInfo(SensorHand);
        } catch (runtime_error& e) {
            logw("No Hand sensor detected.");
            SensorHand = nullptr;
        }
        
        logm("Rack sensor:");
        try {
            SensorRack = new HiTechColorSensor(Multiplexor->NXT1());
            printInitInfo(SensorRack);
        } catch (runtime_error& e) {
            logw("No Rack sensor detected.");
            SensorRack = nullptr;
        }
        if (!SensorRack && !SensorHand && !LeftSensor && !RightSensor)
            throw runtime_error("No sensors detected in adapter!");
        printSystemStat("Sensors", nullptr);
    } catch (runtime_error& e) {
        Multiplexor = nullptr;
        SensorHand = nullptr;
        SensorRack = nullptr;
        LeftSensor = nullptr;
        RightSensor = nullptr;
        printSystemStat("Sensors", e.what());
    }
}
void BedpoSystem::InitServos() {
    try {
        logi("Initializing Servos...");
        ServoDriver = new ServoDriverHT(SERVO_I2C_ADDRESS, I2CPort::B());
        printInitInfo(ServoDriver);
        ServoDriver->Enable(true);
        HandServo = new ContinuousServo(ServoDriver, HAND_SERVO_CHANNEL);
        TowerServo = new PreciseServo(ServoDriver, TOWER_SERVO_CHANNEL);
        TowerServo->SetTiming(3);
        
        printSystemStat("StandardServo", nullptr);
    } catch (runtime_error& e) {
        ServoDriver = nullptr;
        HandServo = nullptr;
        TowerServo = nullptr;
        printSystemStat("StandardServo", e.what());
    }
}

#define SAFE_DELETE(ptr) {try{if(ptr){delete ptr;}}catch(std::exception& e){cout << "!->in destructor: " << e.what() << endl;}}

BedpoSystem::~BedpoSystem() {
    logi("Finalizing myRIO...");
    //
    // Servos
    SAFE_DELETE(ServoDriver);
    SAFE_DELETE(HandServo);
    SAFE_DELETE(TowerServo);
    // Motors
    SAFE_DELETE(Motors);
    SAFE_DELETE(LeftMotor);
    SAFE_DELETE(RightMotor);
    SAFE_DELETE(MotorsDriver);
    // Sensors
    SAFE_DELETE(SensorHand);
    SAFE_DELETE(SensorRack);
    SAFE_DELETE(Multiplexor);
    SAFE_DELETE(RightSensor);
    // Vision
    SAFE_DELETE(Vision);
    // Onboard devices
    //SAFE_DELETE(LedBoard);
    // CLOSE NI MYRIO FPGA
    I2CPort::ResetPorts();
}

void BedpoSystem::Indicate(BedpoSystem::LedIndicatorShow indication) {
    LedBoard->switchOffAll();
    switch (indication) {
        case WaveSingle:
            for (int i = 0; i < 10; ++i) {
                for (int L = 0; L < 4; ++L) {
                    LedBoard->led(L, true);
                    sleep(50);
                    LedBoard->switchOffAll();
                }
            }
            break;
        case AllBlink:
            for (int times = 0; times < 20; ++times) {
                for (int i = 0; i < 4; ++i) {
                    LedBoard->toggle(i);
                }
                sleep(100);
            }
            break;
        case PingPong:
            break;
    }
    LedBoard->switchOffAll();
}
}
}
