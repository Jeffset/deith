#include "wro2016_class.h"
#include "shell.h"

#include <sstream>
#include <syscall.h>
#include <unistd.h>
#include <algorithm>

using namespace std;
using namespace deithvenbot;

deithvenbot::wro2016::CommandShell::CommandShell() {
}
void deithvenbot::wro2016::CommandShell::add(string alias, command action) {
    all_commands[alias] = action;
}
bool wro2016::CommandShell::execute(std::string command) {
    if (command == "\x1b[A") {
        if (mode == Sequence)
            return false;
        printPrompt();
        if (last_command != "") {
            logm(last_command);
            execute(last_command);
        }
        else if (command_sequence.size() != 0) {
            logm("<last sequence>");
            exec_sequence();
        }
        return true;
    }
    if (command.empty())
        return true;
    if (command == "{") {
        if (mode == Sequence)
            return false;
        command_sequence.clear();
        mode = Sequence;
        return true;
    }
    else if (command == "}") {
        mode = Normal;
        return exec_sequence();
    }
    istringstream iss(command);
    string alias, arg;
    iss >> alias;
    auto found_command = all_commands.find(alias);
    if (found_command == all_commands.end())
        return false;
    if (mode == Sequence) {
        command_sequence.push_back(command);
        return true;
    }
    vector<std::string> args;
    while (iss >> arg)
        args.push_back(arg);
    try {
        exec(found_command->second, args);
    } catch (int req) {
        if (req == RESTART_REQ) {
            delete system;
            system = createSystem();
        }
        else
            throw req;
    }
    last_command = command;
    
    return true;
}
struct command_args {
    typename wro2016::CommandShell::command fct;
    wro2016::BedpoSystem* system;
    vector<string> args;
    command_args(const wro2016::CommandShell::command& fct, wro2016::BedpoSystem* system, const vector<string>& args)
            : fct(fct),
              system(system),
              args(args) {}
};
void wro2016::CommandShell::exec(typename wro2016::CommandShell::command cmd, vector<string>& args) {
    try {
        if (!args.empty() && args.back() == "&") {
            args.pop_back();
            auto background_task = [](void* arg) -> void* {
                auto tid = syscall(__NR_gettid);
                logi(string("thread [") + to_string((int) tid) + "] started...");
                command_args* cmd_args = (command_args*) arg;
                try {
                    cmd_args->fct(cmd_args->system, cmd_args->args);
                } catch (invalid_argument& ia) {
                    string err = "invalid argument: \'";
                    err = err + ia.what() + "\'";
                    loge(err);
                }
                catch (unmet_requirement& ur) {
                    string err = "unmet requirement: \'";
                    err = err + ur.what() + "\'";
                    loge(err);
                }
                catch (exception& e) {
                    loge(e.what());
                }
                logi(string("thread [") + to_string((int) tid) + "] finished.");
                delete cmd_args;
                pthread_exit(nullptr);
                return nullptr;
            };
            pthread_t background_thread;
            command_args* cmd_args = new command_args{cmd, system, args};
            pthread_create(&background_thread, nullptr, background_task, cmd_args);
            sleep(50);
            return;
        }
        cmd(system, args);
    }
    catch (invalid_argument& ia) {
        string err = "invalid argument: \'";
        err = err + ia.what() + "\'";
        loge(err);
    }
    catch (unmet_requirement& ur) {
        string err = "unmet requirement: \'";
        err = err + ur.what() + "\'";
        loge(err);
    }
    catch (exception& e) {
        loge(e.what());
    }
}
void wro2016::CommandShell::printPrompt() {
    if (mode == Sequence) {
        printContinuation();
    }
    else {
        if (use_colored_logging)
            cout << "\x1b[33m" << "bedpobot:shell# " << "\x1b[0m";
        else
            cout << "bedpobot:shell# ";
    }
}
void wro2016::CommandShell::printContinuation() {
    if (use_colored_logging)
        cout << "\x1b[33m" << "sequence:> " << "\x1b[0m";
    else
        cout << "sequence:> ";
}
bool wro2016::CommandShell::startup(int argc, char** argv) {
    vector<string> args;
    for (int i = 1; i < argc; ++i) {
        args.emplace_back(argv[i]);
    }
    check_args(args);
    try {
        system = createSystem();
    } catch (std::exception&) {
        // Фатальная ошибка инициализации! просто тихо выйти из проги
        return true;
    }
    if (args.size() > 0) {
        string alias(args[0]);
        auto found_command = all_commands.find(alias);
        if (found_command == all_commands.end()) {
            loge("Invalid startup command!");
            return true; // Ошибка: выйти из программы, ничего не делая
        }
        args.erase(args.begin());
        try {
            exec(found_command->second, args);
        } catch (int req) {
            loge("command '~restart' with invalid startup logic was specified!");
        }
        return true; // Команда выполнена (успешно или с ошибкой), выйти из программы
    }
    else
        return false; // Войти в режим командной строки
}
void wro2016::CommandShell::check_args(vector<string>& args) {
    auto iter = find(args.begin(), args.end(), "-nocolor");
    if (iter != args.end()) {
        use_colored_logging = false;
        args.erase(iter);
    }
    //
    iter = find(args.begin(), args.end(), "-logfile");
    if (iter != args.end()) {
        logfile = new ofstream("/home/admin/deithvenbot.log", ios::app);
        cout.rdbuf(logfile->rdbuf());
        use_colored_logging = false;
        args.erase(iter);
    }
}
wro2016::CommandShell::~CommandShell() {
    if (system)
        delete system;
    if (logfile) {
        cout.rdbuf(cout_buff);
        delete logfile;
    }
}
bool wro2016::CommandShell::exec_sequence() {
    logi("> " + to_string(command_sequence.size()) + " commands in queue");
    for (auto& command : command_sequence) {
        logi(">>> " + command);
        if (!execute(command))
            return false;
    }
    last_command = "";
    return true;
}
wro2016::BedpoSystem* wro2016::CommandShell::createSystem() {
    return new WROBowlingChallenge();
}

