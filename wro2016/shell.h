
#ifndef MYRIO_SHELL_H
#define MYRIO_SHELL_H

#include "bedpo.h"
#include <iostream>
#include <map>

#define RESTART_REQ 666

namespace deithvenbot {
namespace wro2016 {

class unmet_requirement : public std::runtime_error {
public:
    unmet_requirement(const char* msg) : runtime_error(msg) {};
};

class CommandShell {
public:
    using command = std::function<void(BedpoSystem* sys, std::vector<std::string>&)>;
private:
    enum Mode{
        Normal, Sequence
    } mode;
    std::ofstream* logfile = nullptr;
    std::streambuf* cout_buff = std::cout.rdbuf();

    BedpoSystem* system;
    std::map<std::string, command> all_commands;
    std::string last_command;
    std::vector<std::string> command_sequence;
    
    void check_args(std::vector<std::string>& args);
    void exec(command cmd, std::vector<std::string>& args);
    bool exec_sequence();
    BedpoSystem* createSystem();
public:
    CommandShell();
    
    void add(std::string alias, command action);
    bool execute(std::string command);
    void printPrompt();
    void printContinuation();
    bool startup(int argc, char* argv[]);
    virtual ~CommandShell();
};
}
}

#endif //MYRIO_COMMANDS_H
