#include "wro2016_class.h"
#include "task.h"

namespace deithvenbot {
namespace wro2016 {

using namespace std;

void printTime(const Timer& timer) {
    auto time = timer.get_elapsed_ms() / 1000;
    logi(string("total time: ") + to_string(time / 60) + ":" + to_string(time % 60));
}

WROBowlingChallenge::WROBowlingChallenge() {
    loadSettings();
}
void WROBowlingChallenge::loadSettings() {
    /* configuration conf{"/home/admin/work/build/deithvenbot.json"};
     conf.load(SPEED, "normal-mode", "speed");
     conf.load(HIGH_SPEED, "high-mode", "speed");
     conf.load(SLOW_SPEED, "slow-mode", "speed");
     conf.load(WEAK_POWER, "weak-power", "speed");
     //
     conf.load(HighUp, "high-up", "servo");
     conf.load(LowUp, "low-up", "servo");
     conf.load(SlowestUp, "slowest-up", "servo");
     conf.load(SlowestDown, "slowest-down", "servo");
     conf.load(LowDown, "low-down", "servo");
     conf.load(HighDown, "high-down", "servo");*/
}

WROBowlingChallenge::~WROBowlingChallenge() {
}

void WROBowlingChallenge::moveHandByColor(float speed, WaitColorMode mode, WaitColor color, int timeout) {
    HandServo->StartMove(speed);
    SensorHand->Wait(mode, color, timeout);
    HandServo->Stop();
}
void WROBowlingChallenge::rideByColor(sbyte speed, WaitColorMode mode, WaitColor color, HiTechColorSensor* sensor) {
    Motors->RideConstSpeed(speed);
    sensor->Wait(mode, color);
    Motors->StopBrake();
}
void WROBowlingChallenge::tower4Grab(bool wait) {
    TowerServo->SetPosition(10);
}
void WROBowlingChallenge::tower4Compact(bool wait) {
    TowerServo->SetPosition(244);
}
void WROBowlingChallenge::rideFree(int ms) {
    Motors->RideJustPower(WEAK_POWER);
    sleep(ms);
    Motors->StopBrake();
}
void WROBowlingChallenge::rideRolls(float rolls, sbyte speed, bool wait) {
    Motors->RideRolls(rolls, speed, wait);
}
void WROBowlingChallenge::turnCentered(float degrees, bool wait) {
    Motors->TurnCentered(degrees, TURN_SPEED, wait);
}
void WROBowlingChallenge::turnCenteredPrec(float degrees, bool wait) {
    Motors->TurnCentered(degrees, TURN_SPEED_PREC, wait);
}
void WROBowlingChallenge::turnOnWheel(float degrees, DCMotorDifChassis::Wheels wheel, bool wait) {
    Motors->TurnOnWheel(degrees, wheel, TURN_SPEED, wait);
}
void WROBowlingChallenge::turnOnWheelPrec(float degrees, DCMotorDifChassis::Wheels wheel, bool wait) {
    Motors->TurnOnWheel(degrees, wheel, TURN_SPEED_PREC, wait);
}
void WROBowlingChallenge::moveHand4time(float speed, int ms) {
    HandServo->StartMove(speed);
    sleep(ms);
    HandServo->Stop();
}

void WROBowlingChallenge::moveHand4Grab() {
    moveHandByColor(SlowestUp, UntilColor, White, 4_s);
}
void WROBowlingChallenge::centrateBySensors(bool backward) {
    sbyte speed = backward ? -SLOW_SPEED : SLOW_SPEED;
    float delta_tweak = backward ? 3.0_cm : -3.0_cm;
    //
    auto centrator = [ speed, delta_tweak ](DCMotor* motor, AbstractColorSensor* sensor) -> long {
        Timer timer;
        motor->RideConstSpeed(speed);
        sensor->WaitLightness(false);
        motor->RideConstSpeed(-2);
        return timer.get_elapsed_ms();
    };
    for (;;) {
        task tL{centrator, LeftMotor, LeftSensor};
        task tR{centrator, RightMotor, RightSensor};
        auto timeL = tL.join<long>();
        auto timeR = tR.join<long>();
        Motors->StopBrake();
        if (abs(timeL - timeR) < 60)
            break;
        rideRolls(delta_tweak, SLOW_SPEED);
    }
}

//===================================================================
void _handler(AbstractColorSensor* sensor, ContinuousServo* servo, volatile bool* exit_flag) {
    sleep(500);
    logi("hand fixing is enabled");
    while (!*exit_flag) {
        if (sensor->GetEnumedColor() != White) {
            logw("hand is not in place, adjusting...");
            servo->StartMove(140);
            sensor->Wait(UntilColor, White);
            servo->Stop();
            if (sensor->GetEnumedColor() != White) {
                logw("little over position, fixing...");
                servo->StartMove(125);
                sleep(130);
                servo->Stop();
            }
            logok("hand is now in place");
            sleep(500);
        }
        sleep(100);
    }
    logi("hand fixing is now disabled");
}

void WROBowlingChallenge::indicateTask() {
    LedBoard->switchOffAll();
    switch (taskType) {
        case TaskType::Unknown:
            Indicate(WaveSingle);
            break;
        case TaskType::Obstacle:
            LedBoard->led(2, true);
        case TaskType::SimpleGreen:
            LedBoard->led(1, true);
        case TaskType::Simple:
            LedBoard->led(0, true);
    }
}

HandFixator::HandFixator(AbstractColorSensor* sensor, ContinuousServo* servo)
        : exit_flag(false), sensor(sensor), servo(servo),
          handler(_handler, sensor, servo, &exit_flag) {
}
HandFixator::~HandFixator() {
    exit_flag = true;
    handler.join();
}
    
}
}
