#ifndef MYRIO_WRO_CLASS_H
#define MYRIO_WRO_CLASS_H

#include "bedpo.h"
#include "task.h"

namespace deithvenbot {
namespace wro2016 {

class HandFixator{
    bool exit_flag;
    AbstractColorSensor* sensor;
    ContinuousServo* servo;
    task handler;
public:
    HandFixator(AbstractColorSensor* sensor, ContinuousServo* servo);
    ~HandFixator();
};

/**
 * @brief Главный класс для выполнения задачи WRO Advanced Bowling Challenge.
 * Вся конфигурация (значения) задается (читаются из конфигурационных файлов здесь).
 * Сюда необходимо перенести все методы из класса bedpo, которые имеют отношение
 * к выполнению соревновательной задачи.
 */
class WROBowlingChallenge : public BedpoSystem {
public:
    WROBowlingChallenge();
public:
    enum class ThrowingPosition {
        Left = 0, Center = 1, Right = 2
    };
    enum class TrianglePos {
        Unknown = -1, Left = 0, Center = 1, Right = 2
    };
    TrianglePos pinPos = TrianglePos::Unknown;
    TrianglePos greenPos = TrianglePos::Unknown;
    enum class TaskType {
        Unknown = -1, Simple = 0, SimpleGreen = 1, Obstacle = 2
    } taskType = TaskType::Unknown;
    TrianglePos rightObstaclePos = TrianglePos::Unknown;
    int balls_in_rack = 3;
    int ballNumber = 0;
    bool wasStrike = false, wasSpare = false;
    bool almostStrike = false;
    bool split = false;
    bool errorCondition = false;
    bool green_throw = false;

public:
    void rideRolls(float rolls, sbyte speed, bool wait = true);
    void turnCentered(float degrees, bool wait = true);
    void turnCenteredPrec(float degrees, bool wait = true);
    void turnOnWheel(float degrees, DCMotorDifChassis::Wheels wheel, bool wait = true);
    void turnOnWheelPrec(float degrees, DCMotorDifChassis::Wheels wheel, bool wait = true);
    void rideFree(int ms);
    //void moveHandByColor(float speed, WaitColorMode mode, WaitColor color);
    void moveHandByColor(float speed, WaitColorMode mode, WaitColor color, int timeout);
    void moveHand4time(float speed, int ms);
    void moveHand4Grab();
    void rideByColor(sbyte speed, WaitColorMode mode, WaitColor color, HiTechColorSensor* sensor);
    void tower4Grab(bool wait = true);
    void tower4Compact(bool wait = true);
    void throwBall();
    void aim(std::function<int(VisSpots&)> aimer);
    void centrateBySensors(bool backward);
    bool taskBreakCondition();
    bool isFullTriangle();
    
    void completeTask();
    
    void loadSettings();
    
    void firstLook();
    void secondLook();
    
    void check4StrikeOrSpare();
    void ride2ThrowingPosition(ThrowingPosition position);
    void return2Center(ThrowingPosition position);
    void return2Base();
    
    void indicateTask();
    
    VisRectangle leftInvestRegion() const;
    VisRectangle rightInvestRegion() const;
    VisRectangle aimingRegion() const;
    VisRectangle aimingRegionExt() const;
    
    void mainProgram();
    virtual ~WROBowlingChallenge();
};

extern sbyte SPEED;
extern sbyte TURN_SPEED;
extern sbyte TURN_SPEED_PREC;
extern sbyte HIGH_SPEED;
extern sbyte SLOW_SPEED;
extern sbyte WEAK_POWER;
    
}
}
#endif //MYRIO_WRO_CLASS_H
