#include "wro2016_class.h"

#include <iostream>
#include <algorithm>

namespace deithvenbot {
namespace wro2016 {

using namespace std;

#define WAIT true
#define PARALLEL false

sbyte SPEED = 24;
sbyte TURN_SPEED = 20;
sbyte TURN_SPEED_PREC = 12;
sbyte HIGH_SPEED = 32;
sbyte SLOW_SPEED = 14;
sbyte WEAK_POWER = 9;
const float rideBack = -18.0_cm;


using ThrowingPos = WROBowlingChallenge::ThrowingPosition;
using PinPos = WROBowlingChallenge::TrianglePos;

#define L ThrowingPos::Left
#define C ThrowingPos::Center
#define R ThrowingPos::Right
ThrowingPos throwing_positions[3][3][2] = {// [task][pin_pos][(ballNumber-1)%2]
        {{L, L}, {C, C}, {R, R}},
        {{L, L}, {C, C}, {R, R}},
        {{R, R}, {R, R}, {R, L}}
};
#undef L
#undef C
#undef R

function<int(VisSpots&)> getCentralAimer() {
    logi("<using central aimer>");
    return [](VisSpots& spots) -> int {
        if (spots.empty())
            throw runtime_error("in aimer: no spots detected!");
        return (spots.front().pos + spots.back().right()) / 2;
    };
}
function<int(VisSpots&)> getMaxSpotAimer() {
    logi("<using max spot aimer>");
    return [](VisSpots& spots) -> int {
        if (spots.empty()) {
            throw runtime_error("in aimer: no spots detected!");
        }
        return max_element(spots.begin(), spots.end(),
                           [](const VisSpot& s1, const VisSpot& s2) -> bool {
                               return s1.length < s2.length;
                           })->center();
    };
}
function<int(VisSpots&)> getDeltaAimer(int delta) {
    if (delta < 0) {
        logi("<using {right - " + to_string(-delta) + "} aim>");
        return [ delta ](VisSpots& spots) {
            return (spots.back().right() + delta);
        };
    }
    else {
        logi("<using {left + " + to_string(delta) + "} aim>");
        return [ delta ](VisSpots& spots) {
            return (spots.front().pos + delta);
        };
    }
}

struct tweak_struct {
    float wheel_rot;
    float hand_rot;
};

tweak_struct tweaks[] = {
        {0.f,   40.f}, // -2
        {0.f,   30.f}, // -1
        {8.0f,  38.f}, // 0
        {10.0f, 20.f}, // +1
        {14.0f, 29.f}  // +2
};

void printTime(const Timer& timer);

tweak_struct calcPreparation(ThrowingPos tp, PinPos pp) {
    int dir = (int) pp - (int) tp;
    return tweaks[dir + 2];
}

void WROBowlingChallenge::throwBall() {
    HandServo->StartMove(LowDown);
    sleep(2000);
    moveHand4time(SlowestUp, 1600);
}
void WROBowlingChallenge::mainProgram() {
	taskType = TaskType::Simple;
    Timer mainTimer;
    logi("-------------- main wro2016 program start --------------");
    tower4Grab(PARALLEL);                           // Башню в забирающее положение
    moveHandByColor(SlowestUp, UntilColor, White, 4_s);  // Руку временно в горизонтальное положение
    moveHand4time(SlowestDown, 650);                // Руку в вертикальное положение
    //----- Начинаем ехать -------
    centrateBySensors(false);                  // До края стартовой зоны по сенсорам
    rideRolls(60.0_cm, SPEED);                   // Едем к линии
    centrateBySensors(false);                  // Центруемся по реф-линии (слева)
    //--------------------@@@@@@@@@@@@@-----------------------------
    govno:
    /// @state ------------ Теперь на реф-лини ----------------
    try {
        firstLook(); //~~~~~~ FIRST LOOK ~~~~~~~~
    } catch (runtime_error& e) {
        loge("in first look: "_str + e.what());
        taskType = TaskType::SimpleGreen;
        pinPos = TrianglePos::Center;
        errorCondition = true;
        Indicate(AllBlink);
    }
    if (taskType == TaskType::Unknown) { // Если одного раза недостаточно, нужно смотреть еще раз
        logi("second look is needed");
        rideRolls(-18.0_cm, SPEED); //  Отъезжаем от линии немного
        turnOnWheel(90, DCMotorDifChassis::Right); // Поворачиваем вокруг правого носом к рэку
        rideRolls(58.0_cm, SPEED); // Едем много вдоль линии
        turnCentered(-90); // Поворачиваемся к кеглям
        centrateBySensors(false); // Центруемся
        //~~~~~~~~~~ SECOND LOOK ~~~~~~~~~~~
        try {
            secondLook(); // Смотрим второй раз
        } catch (runtime_error& e) {
            loge("in second look: "_str + e.what());
            taskType = TaskType::Simple;
            pinPos = TrianglePos::Center;
            errorCondition = true;
            Indicate(AllBlink);
        }
        //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        turnOnWheel(45, DCMotorDifChassis::Left);
        rideRolls(-44.0_cm, HIGH_SPEED);
        turnCentered(45);
        //
        moveHandByColor(SlowestUp, UntilColor, White, 4_s); // Руку в положения для взятия
        rideFree(3_s); // Центровка по рэку
    }
    else { // Если первого раза достаточно (сейчас мы на реф-линии слева)
        logi("I've seen enough, let's do it");
        rideRolls(-52.0_cm, HIGH_SPEED);  // отъезжаем назад прилично
        turnOnWheel(90, DCMotorDifChassis::Right); // поворачиваем к рэку
        //
        moveHandByColor(SlowestUp, UntilColor, White, 4_s); // Руку в положения для взятия
        rideRolls(60.0_cm, HIGH_SPEED); // Едем к рэку
        rideFree(2_s); // Центровка по рэку
    }
    /// @state -------- Теперь мы уткнуты носом в рэк --------------
    for (;;) {
        if (taskBreakCondition()) // Если выполнено условие завершения таски
            break; // выходим из цикла
        logm("main loop begin");
        //
        turnOnWheel(-46, DCMotorDifChassis::Right); // 45
        rideRolls(5.0_cm, SLOW_SPEED); // Подъехать
        turnOnWheel(-45, DCMotorDifChassis::Right); // 45
        rideByColor(SLOW_SPEED, WhileColor, Black, SensorRack); // Вперед до рэка
        rideByColor(-SLOW_SPEED, UntilColor, Black, SensorRack); // Назад до черного
        rideRolls(4.7_cm - (ballNumber * 3.5_cm), SLOW_SPEED); // НАСТРОЕЧНОЕ ЗНАЧЕНИЕ
        // ---------------------- Забор шарика ---------------------------------
        moveHand4time(HighDown, 600); // Берем шарик
        HandServo->StartMove(LowDown); // Руку медленнее
        rideRolls(8.0_cm, SLOW_SPEED, PARALLEL); // Проезжаем
        SensorHand->Wait(UntilColor, {Violet, White}); // Ждать шара
        HandServo->Stop(); // Дождались шара
        tower4Compact(PARALLEL); // Руку компактно сложить спереди...
        rideByColor(SLOW_SPEED, UntilColor, Black, SensorRack); // ...и проехать вперед до конца рэка
        ++ballNumber;
        // ---- Шарик у нас -------
        completeTask();
        // ---- Шарика у нас нет --
        /// @state --------- уткнуты носом в рэк ----------
        if ((ballNumber == 1 && wasStrike) || (ballNumber == 2 && wasSpare)) {
            logi("I've strike or spare => I have to watch again");
            rideRolls(-70.0_cm, HIGH_SPEED); // От рэка едем много назад
            turnOnWheel(-90, DCMotorDifChassis::Right); // Поворот к линии
            moveHand4time(SlowestDown, 650);   // Руку в вертикальное положение
            centrateBySensors(false);
            goto govno;
        }
        else {
            logok("position of pins is known");
        }
    }
    return2Base();
    printTime(mainTimer);
    logok("--------- program finished ----------");
    LedBoard->switchOnAll();
    sleep(60_s);
    Indicate(WaveSingle);
    LedBoard->switchOffAll();
}
void WROBowlingChallenge::completeTask() {
    green_throw = false; // Очищаем флаг зеленого удара
    tweak_struct tweak;
    ThrowingPos target_pos = ThrowingPosition::Center; // Центр тут просто так, на всякий
    //
    if (taskType == TaskType::SimpleGreen &&
        !isFullTriangle() && !(ballNumber == 2 && almostStrike)) {
        // Если задача с грином, полный треугольник, или надо добить спейр
        green_throw = true; // Тогда бьем В ЗЕЛЕНУЮ
        target_pos = (ThrowingPos) greenPos; // кидаем туда же, где и кегли
        tweak = calcPreparation(target_pos, greenPos); // Вычисляем угол поворота корпуса и башни по позиции кегель
        logi("Now it's time to use green pin");
    }
        // Если это обстакл и он справа, то выбираем две позции (+) и (-)
    else if (taskType == TaskType::Obstacle && pinPos == TrianglePos::Right) {
        if (rightObstaclePos == TrianglePos::Right) {
            switch (ballNumber) {
                case 1:
                    target_pos = ThrowingPosition::Right;
                    break;
                case 2:
                    target_pos = ThrowingPosition::Left;
                    break;
                case 3:
                    target_pos = ThrowingPosition::Right;
            }
        }
        else {
            switch (ballNumber) {
                case 1:
                    target_pos = ThrowingPosition::Left;
                    break;
                case 2:
                    target_pos = ThrowingPosition::Left;
                    break;
                case 3:
                    target_pos = ThrowingPosition::Left;
            }
        }
        // Вычисляем соответствующий угол поворота корпуса и башни
        tweak = calcPreparation(target_pos, TrianglePos::Right); // Тут по любому направо
    }
    else { // В любом другом случае, сморим по табличке из костылей
        target_pos = throwing_positions[(int) taskType][(int) pinPos][(ballNumber - 1) % 2];
        tweak = calcPreparation(target_pos, pinPos); // Вычисляем соответствующий угол поворота корпуса и башни
    }
    logi(string("shifting tweak: wheels += ") +
         to_string(tweak.wheel_rot) + ", servo += " + to_string(tweak.hand_rot));
    // ---------- Позиционирование и подготовка положения ------------------
    ride2ThrowingPosition(target_pos); // Едем в нужную огневую позоцию
    turnOnWheelPrec(tweak.wheel_rot, DCMotorDifChassis::Left, PARALLEL); // Поворачиваем корпус робота (если нужно)...
    TowerServo->SetPosition(tweak.hand_rot);// ... и поворачиваем башню
    Motors->WaitForMotors(); // Конец поворота корпуса
    Motors->Stop();
    moveHand4time(SlowestUp, 35); // Чисто для гарантии, что конец руки не попадет в поле зрения камеры
    sleep(1_s);
    // ------------- Целимся ------------------------
    switch (taskType) {
        case TaskType::Simple:
            if (isFullTriangle()) // Если у нас полный треугольник
                aim(getMaxSpotAimer()); // то кидаем в центр
            else {
                aim(getMaxSpotAimer()); // иначе кидаем в макс спот
            }
            break;
        case TaskType::SimpleGreen: // Если таска с зеленой
            if (green_throw) {
                logi("aiming to green pin");
                aim(getMaxSpotAimer()); // целимся в зеленую по макс споту
            }
            else {
                aim(getMaxSpotAimer()); // Белые кегли в макс спот
            }
            break;
        case TaskType::Obstacle: {
            int shift = (int) pinPos - (int) target_pos;
            if (shift == -2) { // Если справа в самый левый
                if (isFullTriangle())
                    aim(getDeltaAimer(-22));
                else
                    aim(getDeltaAimer(+53));
            }
            else if (shift == -1) { // Если справа в центр
                if (isFullTriangle())
                    aim(getDeltaAimer(-22));
                else
                    aim(getDeltaAimer(+56));
            }
            else if (shift > 0) { // Если слева в право (для right- и right+ второй удар)
                if (isFullTriangle())
                    aim(getDeltaAimer(+28));
                else
                    aim(getDeltaAimer(-60));
            }
            else { // shift == 0 // Если справа в право (для right+ первый удар)
                if (isFullTriangle())
                    aim(getDeltaAimer(-22));
                else
                    aim(getDeltaAimer(+52));
            }
            break;
        }
        case TaskType::Unknown:
            break;
    }
    // Кидаем шар
    throwBall();
    // Анализируем результаты
    check4StrikeOrSpare();
    
    // Отворачиваем назад колесами и рукой
    turnOnWheelPrec(-tweak.wheel_rot, DCMotorDifChassis::Left, PARALLEL); // Поворачиваем корпус робота (если нужно)...
    // Возвращаемся в центр
    return2Center(target_pos);
}
void WROBowlingChallenge::aim(function<int(VisSpots&)> aimer) {
    int iteration = 0;
    auto aimingReg = (taskType == TaskType::Obstacle) ? aimingRegionExt() : aimingRegion();
    float factor = 1.0f;
    try {
        do {
            // Общее получение кадра
            Vision->See();
            if (green_throw) {
                // Если целимся в зеленую, то думаем о зеленом
                Vision->ThinkOfGreen(aimingReg);
            }
            else {
                // Если в белые, то смотрим на белые
                Vision->ThinkOfWhite(aimingReg);
                // И считаем шифт
                Vision->CalcShift();
            }
            int upper_line, lower_line;
            // Ищем линии
            bool found = Vision->FindLines(upper_line, lower_line);
            if (!found) { // Если не нащли, то просто типа прицелились - кидаем
                Indicate(AllBlink);
                loge("no lines found! just throwing");
                return;
            }
            // Извлекаем пятна
            auto spots = Vision->ExtractSpots(lower_line, true);
            // Сразу же сохраняем
            Vision->Save("./"_str + to_string(ballNumber) + "/" + to_string(iteration) + ".jpg");
            // Если сейчас задача с зеленой, но удар не в зеленую
            if (taskType == TaskType::SimpleGreen && !green_throw) {
                logm("green_throw+ extract spots"); // то убираем зеленую
                auto green_spot = Vision->See(1)->ThinkOfGreen(aimingRegion())->ExtractSpots(lower_line, false);
                Vision->RemoveGreen(spots, green_spot);
            }
            if (spots.empty()) { // Если пятен нет, то см аналог. случай выше
                Indicate(AllBlink);
                loge("no spots detected! just throwing");
                return;
            }
            // Целимся аимером
            factor = Vision->CalculateAimFactor(aimer(spots));
            auto eps = factor>0.f?factor:-factor;
            auto sign = eps ? factor / eps : 0.f;
            float delta;
            if (iteration > 15) { // Если слишком долго целимся, то просто кидаем.
                return;
            }
            else if (eps > 0.5f) {
                delta = 15.f;
            }
            else if (eps > 0.2f) {
                delta = 7.f;
            }
            else if (eps > 0.15f) {
                delta = 3.f;
            }
            else if (eps > 0.1f) {
                delta = 2.f;
            }
            else
                delta = 1.f;
            cout << "Factor = " << factor << "\n";
            TowerServo->AdvancePosition(-delta * sign);
            iteration++;
        } while ((factor>0.f?factor:-factor) > 0.01f);
    } catch (runtime_error& e) {
        loge(e.what());
        Vision->Save("error.jpg");
    }
}
//! \brief Исследование кегель слева (первый просмотр)
void WROBowlingChallenge::firstLook() {
    logi("---------- first look -----------");
    int u_line, l_line;
    // ---------- ЗЕДЕНЫЕ КЕГЛИ ----------------
    Vision->See()->ThinkOfGreen(leftInvestRegion());
    VisSpots green_spot; // пятно(пятна) для возможной зеленки
    if (Vision->FindLines(u_line, l_line)) { // Если нашло линии, то есть зеленая
        cout << "line = " << l_line << endl;
        green_spot = Vision->ExtractSpots(l_line, true); // Вычленяем зеленые пятна
        if (!green_spot.empty()) { // Если они есть (что не странно)
            bool mistake = true; // Не зеленка
            for (auto& s : green_spot) // Для каждого пятна
                if (s.length > 7) { // Если размер пятна больше порога, то это кегля и ок
                    mistake = false;
                    break;
                }
            if (!mistake) { // Если не ошибка
                logok("green is detected! Task is GreenSimple");
                taskType = TaskType::SimpleGreen; // сразу знает что таска симпл-грин
                if (green_spot.size() > 1) // Если пятен несколько
                    logw("Green spots are multiple");
                auto green_p = green_spot.front().center(); // берем центр зеленки
                if (green_p <= 105) { // ЛЕВОЕ ПОЛОЖЕНИЕ ЗЕЛЕНКИ
                    greenPos = TrianglePos::Left;
                    logi("green: left");
                }
                else if (green_p <= 180) { // ЦЕНТРАЛЬНОЕ ПОЛОЖЕНИЕ ЗЕЛЕНКИ
                    greenPos = TrianglePos::Center;
                    logi("green: center");
                }
                else { // ИНАЧЕ ТУПО ПРАВОЕ ПОЛОЖЕНИЕ ЗЕЛЕНКИ
                    greenPos = TrianglePos::Right;
                    logi("green: right");
                }
                cout << green_spot.size() << ' ' << green_spot.front().length << ' ' << green_spot.front().pos << endl;
                Vision->Save("green-test.jpg");
            }
            else {
                logw("some greenish noise present");
            }
        }
    }
    // -------- БЕЛЫЕ КЕГЛИ ----------------
    Vision->See(1)->ThinkOfWhite(leftInvestRegion()); // Смотрим на белые (один кадр)
    if (!Vision->FindLines(u_line, l_line)) // Если не нашел линию, то кидаем исключение
        throw runtime_error(
                "can't detect lines (no spots detected)!"); // которое будет словлено сверху и errorCondition
    cout << "line = " << l_line << endl;
    auto lower_spots = Vision->ExtractSpots(l_line, true); // извлекаем пятна
    if (lower_spots.empty())
        throw runtime_error("no spots detected!"); // если пятен нет, то это опять же хреново, см выше
    if (taskType ==
        TaskType::SimpleGreen) // Если мы сверху задетектили зеленку, то убераем ее по вычисленному сверху пятну
        Vision->RemoveGreen(lower_spots, green_spot);
    int left_edge = lower_spots.front().pos; // Левая граница
    cout << "left edge = " << left_edge << endl;
    if (left_edge <= 80) { // ЛЕВОЕ ПОЛОЖЕНИЕ БЕЛЫХ
        pinPos = TrianglePos::Left;
        logi("pins: left");
    }
    else if (left_edge <= 130) { // ЦЕНТРАЛЬНОЕ ПОЛОЖЕНИЕ БЕЛЫХ
        pinPos = TrianglePos::Center;
        logi("pins: center");
    }
    else { // ПРАВОЕ ПОЛОЖЕНИЕ БЕЛЫХ
        pinPos = TrianglePos::Right;
        logi("pins: right");
    }
    //--------------------- Ну как бы дальше тупо для ФИНАЛА --------------
    if (taskType == TaskType::Unknown) { // Если еще не знаем таску
        if (pinPos ==
            TrianglePos::Right) { // Если кегли справа, то надо проаналить на наличие правого обстакла и его тип
            int length = lower_spots.back().right() - lower_spots.front().pos; // Смотрим длину всех пятен
            cout << "lower length = " << length << endl;
            if (length > 85) { // ПОРОГ ДЛИНЫ для наличия обстакла
                taskType = TaskType::Obstacle;
                if (length <= 115) { // RIGHT(+)
                    rightObstaclePos = TrianglePos::Right;
                    logok("task is Obstacle (right+)");
                }
                else { // RIGHT(-)
                    rightObstaclePos = TrianglePos::Left;
                    logok("task is Obstacle (right-)");
                }
            }
            else { // Если меньше порога обстакла, то ПРАВЫЙ СИМПЛ
                taskType = TaskType::Simple;
                logok("task is Simple");
            }
        }
    }
    else if (taskType != TaskType::SimpleGreen) {
        logok("task was known before");
    }
    Vision->Save("debug_left.jpg");
    indicateTask();
    logi("-----------------------------");
}
//! \brief Дополнительные исследования справа
void WROBowlingChallenge::secondLook() {
    // Здесь нам нужно только аналить белые
    logi("---------- second look -----------");
    Vision->See()->ThinkOfWhite(rightInvestRegion()); // Аналим белые
    int u_line, l_line;
    if (!Vision->FindLines(u_line, l_line)) { // Ищем линии
        throw runtime_error("no lines on white spots detected!"); // еррор условие
    }
    auto l_spots = Vision->ExtractSpots(l_line, true);
    if (l_spots.empty()) // Если нет пятен - еррор условие
        throw runtime_error("no white spots detected!");
    int length = l_spots.back().right() - l_spots.front().pos; // "Диаметр" мно-ва пятен
    cout << "lower length = " << length << endl;
    if (length > 85) { // ПОРОГ ДЛЯ ОБСТАКЛА
        taskType = TaskType::Obstacle;
        logok("task is Obstacle");
    }
    else { // ИНАЧЕ СИМПЛ
        taskType = TaskType::Simple;
        logok("task is Simple");
    }
    Vision->Save("debug_right.jpg");
    indicateTask();
    logi("--------------------------------");
}
bool WROBowlingChallenge::taskBreakCondition() {
    if (wasStrike || wasSpare)
        return ballNumber >= 3;
    else
        return ballNumber >= 2;
}
void WROBowlingChallenge::ride2ThrowingPosition(ThrowingPosition position) {
    switch (position) {
        case ThrowingPosition::Left: {
            turnOnWheel(90, DCMotorDifChassis::Left); // Повернуться задом к полю
            rideRolls(-47.0_cm, SPEED); // Едем задом вдоль линии налево почти до края
            turnOnWheel(-90, DCMotorDifChassis::Right); // Поворачиваем направо носом к кеглям
            rideRolls(35.0_cm, SPEED); // Едем вперед почти до линии
            centrateBySensors(false); // Центруемся
            rideRolls(rideBack, SLOW_SPEED); // Райдбэк
            break;
        }
        case ThrowingPosition::Center: {
            turnOnWheel(90, DCMotorDifChassis::Left); // Повернуться задом к полю
            rideRolls(-40.0_cm, SPEED);  // Едем задом вдоль линии налево до центра
            turnOnWheel(-90, DCMotorDifChassis::Left); // Поворачиваем направо носом к кеглям
            centrateBySensors(false); // Центруемся
            rideRolls(rideBack, SLOW_SPEED); // Райдбэк
            break;
        }
        case ThrowingPosition::Right: {
            rideRolls(10.0_cm, SPEED); // просто от рэка едем вперед
            centrateBySensors(false); // центруемся
            rideRolls(rideBack, SLOW_SPEED); // Райдбэк
            break;
        }
    }
}
void WROBowlingChallenge::return2Center(ThrowingPosition position) {
    tower4Compact();
    switch (position) {
        case ThrowingPosition::Left:
            turnOnWheel(-45, DCMotorDifChassis::Right);
            rideRolls(-30.0_cm, HIGH_SPEED);
            turnCentered(135, true);
            //
            tower4Grab();
            moveHand4Grab();
            rideRolls(20.0_cm, HIGH_SPEED); // Едем к рэку
            rideFree(3_s); // Центровка по рэку
            break;
        case ThrowingPosition::Center:
            rideRolls(-10.0_cm, SLOW_SPEED);
            turnOnWheel(90, DCMotorDifChassis::Left);
            //
            tower4Grab();
            moveHand4Grab();
            rideRolls(40.0_cm, HIGH_SPEED); // Едем к рэку
            rideFree(2_s); // Центровка по рэку
            break;
        case ThrowingPosition::Right:
            turnOnWheel(45, DCMotorDifChassis::Left);
            rideRolls(-23.0_cm, HIGH_SPEED);
            turnCentered(45);
            //
            tower4Grab();
            moveHand4Grab();
            rideFree(2_s); // Центровка по рэку
            break;
    }
}
void WROBowlingChallenge::return2Base() {
    logok(">--- returning to base <---");
    rideRolls(-70.0_cm, HIGH_SPEED); // От рэка едем много назад
    turnOnWheel(-90, DCMotorDifChassis::Right); // Поворот к старту
    centrateBySensors(true); //  Центрируемся
    rideRolls(-9.0_cm, SLOW_SPEED); // Немного еще проезжаем в старт
    moveHand4time(SlowestDown, 600); // Руку в начальное положение
}
void WROBowlingChallenge::check4StrikeOrSpare() {
    if (errorCondition) {
        logw("working under error condition -> no analysis though");
        return;
    }
    if (green_throw) {
        logi("green was used, need not to check strike/spare");
        return;
    }
    if (ballNumber == 3) {
        logok("no need to check results in the 3rd ball");
        return;
    }
    //if (ballNumber == 2 && wasStrike) {
    //    logi("second strike is impossible:) (sunset condition)");
    //    return;
    //}
    logi("waiting for ball to hit pins...");
    // Ждем пока шарик не докатится
    sleep(2_s);
    logi("analyzing results (for strike/spare)...");
    // берем предыдущее смещение
    int last_shift = Vision->getVertShift();
    Vision->See()->ThinkOfWhite(aimingRegion()); // Смотрим на белые
    Vision->CalcShift(); // вычисляем новый шифт
    auto target_line =
            Vision->getCachedUpperLine() - (last_shift - Vision->getVertShift()); // Вычисляем сдвинутую линию
    cout << "used (reshifted) line = " << target_line << endl;
    auto spots = Vision->ExtractSpots(target_line, true); // Выделяем (возможно смешанные) пятна по линии
    Vision->Save("./"_str + to_string(ballNumber) + "/strike.jpg");
    if (taskType == TaskType::SimpleGreen) { // Если зеленая таска (но кидаем в белые по предусловию)
        logm("need to check green intersection");
        auto green_spots = Vision->See(1)->ThinkOfGreen(aimingRegion())
                                 ->ExtractSpots(target_line, false); // Еще смотрим на зеленые
        Vision->RemoveGreen(spots, green_spots); // Убираем возможную зеленку из пятен
    }
    if (spots.size() == 1 && spots.front().length <= 23) { // Если одно "маленькое" пятно
        almostStrike = true;
        logok("almost strike/spare state detected!"); // Почти страйк(спейр)
        Indicate(WaveSingle);
    }
    else if (spots.size() >= 2) { // Если больше двух
        auto max_gap = Vision->GetMaxGap(spots); // Вычленяем макс промежуток
        if (max_gap >= 25) {
            split = true;
            logw("split state was detected!");
        }
    }
    if (spots.empty()) { // Если пятен нет
        if (ballNumber == 1) {
            wasStrike = true;
            logok("strike was detected");
        }
        else if (ballNumber == 2) {
            wasSpare = true;
            logok("spare(or second strike:) was detected");
        }
        Indicate(AllBlink);
    }
    else
        logi("no strike/spare detected");
}

#define YYY 340

VisRectangle WROBowlingChallenge::leftInvestRegion() const {
    return VisRectangle(/*X=*/270 - 50,/*Y=*/ YYY, /*Width=*/280,/*Height=*/ 107);
}
VisRectangle WROBowlingChallenge::rightInvestRegion() const {
    return VisRectangle(/*X=*/100 - 40,/*Y=*/ YYY, /*Width=*/280,/*Height=*/ 107);
}
VisRectangle WROBowlingChallenge::aimingRegion() const {
    return VisRectangle(/*X=*/275 + 10,/*Y=*/ YYY, /*Width=*/155 - 18,/*Height=*/ 107);
}
VisRectangle WROBowlingChallenge::aimingRegionExt() const {
    return VisRectangle(/*X=*/270 + 10,/*Y=*/ YYY, /*Width=*/165 - 18,/*Height=*/ 107);
}

bool WROBowlingChallenge::isFullTriangle() {
    return ballNumber == 1 || (ballNumber == 2 && wasStrike) || (ballNumber == 3 && wasSpare);
}
    
}
}
