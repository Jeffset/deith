/*
 *================ main wro program ================
 */

#include "bedpo.h"
#include "shell.h"
#include "commands.h"
#include "wro2016_class.h"


using namespace std;
using namespace deithvenbot;
using namespace deithvenbot::wro2016;


void initShellCommands(CommandShell& shell) {
    shell.add("exit", cmd_exit);
    shell.add("stop", cmd_exit);
    shell.add("restart", cmd_restart);
    //
    shell.add("blink", cmd_blink);
    shell.add("audio", cmd_audio);
    //
    shell.add("color", cmd_color);
    shell.add("sensor", cmd_color);
    shell.add("analog", cmd_analog);
    shell.add("ev3color", cmd_ev3color);
    shell.add("ride", cmd_ride);
    shell.add("mstop", cmd_mstop);
    shell.add("turn", cmd_turn);
    shell.add("cservo", cmd_cservo);
    shell.add("pservo", cmd_pservo);
    shell.add("sleep", cmd_sleep);
    shell.add("accel", cmd_accel);
    shell.add("centrate", cmd_centrate);
    shell.add("see_left", cmd_see_left);
    shell.add("see_right", cmd_see_right);
    shell.add("see_green", cmd_see_green);
    shell.add("fixhand", cmd_fixhand);
    shell.add("reload_set", cmd_reload_set);
    shell.add("manip", cmd_manipulator);
    //
    shell.add("launch", cmd_launch);
}

int main(int argc, char* argv[]) {
    CommandShell shell;
    initShellCommands(shell);
    //
    if (shell.startup(argc, argv))
        return 0;
    //
    string command;
    shell.printPrompt();
    while (getline(cin, command)) {
        try {
            bool found = shell.execute(command);
            if (!found) {
                loge("command(s) not found!");
            }
        } catch (int req) {
            if (req == EXIT_REQ)
                break;
        }
        shell.printPrompt();
    }
    logi("Exiting program");
}
