//
// Created by marco on 30/01/17.
//

#include "cursed_ui.h"

#ifdef MYRIO
#include "ncurses/ncurses.h"
#else

#include "ncurses.h"

#endif


#include "elements.h"

#include <stack>
#include <ncurses.h>
#include <cstring>

namespace ncursed {


//~~~~~~~~~~ Cursed UI members ~~~~~~~~~~~~~~~~~~~
bool in_cursed_mode = false;
bool draw_terminal_border = false;
bool modal_dialog_mode = false;

std::list<scene*> all_scenes; // for resource handling

std::stack<scene*> scene_stack; // scene stack (head is current)
scene* active_scene = nullptr; // for scene creation (not rendering)
//~~~~~~~~~~ Cursed UI members END ~~~~~~~~~~~~~~~

// ---- util ----
template<class T, class B>
T* check_type(B* obj) {
    auto casted = dynamic_cast<T*>(obj);
    if (casted)
        return casted;
    throw std::runtime_error("UI: type mismatch!");
};

//**************** SINGLETON ************************
UI::UI() {
    initscr();
    start_color();
    keypad(stdscr, true);
    noecho();
    mousemask(ALL_MOUSE_EVENTS, nullptr);
    cbreak();
    //
    init_pair((short) color::White, COLOR_WHITE, COLOR_BLACK);
    init_pair((short) color::Red, COLOR_RED, COLOR_BLACK);
    init_pair((short) color::Green, COLOR_GREEN, COLOR_BLACK);
    init_pair((short) color::Blue, COLOR_BLUE, COLOR_BLACK);
    init_pair((short) color::Cyan, COLOR_CYAN, COLOR_BLACK);
    init_pair((short) color::Yellow, COLOR_YELLOW, COLOR_BLACK);
    init_pair((short) color::Magenta, COLOR_MAGENTA, COLOR_BLACK);
    //
    init_pair(BUTTON_BORDER, COLOR_CYAN, COLOR_BLACK);
    init_pair(CHECK_BOX_CHECKED, COLOR_GREEN, COLOR_BLACK);
}
UI::~UI() {
    endwin();
    for (scene* s : all_scenes)
        delete s;
    all_scenes.clear();
    scene_stack = std::stack<scene*>();
    in_cursed_mode = false;
}
UI UI::cursed_gui() {
    if (in_cursed_mode)
        throw std::runtime_error("UI: already in cursed mode!");
    in_cursed_mode = true;
    return UI();
}
//*****************************************************
//---------------- BEGIN FUNCTIONS --------------------
handle UI::begin_scene(bool border, bool cursor) {
    auto new_scene = new scene();
    new_scene->show_border = border;
    new_scene->show_cursor = cursor;
    all_scenes.push_back(new_scene);
    active_scene = new_scene;
    return new_scene;
}
handle UI::begin_frame(int x, int y, int width, int height, const char* title) {
    if (!active_scene)
        throw std::logic_error("UI: no active scene!");
    auto h = new frame(x, y, width, height);
    h->text = title;
    h->on_click = [](handle f, int mx, int my) {
        auto $this = dynamic_cast<frame*>(f);
        for (auto e : $this->elems) {
            auto cl = dynamic_cast<clickable*>(e);
            if (cl) {
                if (cl->check_click(mx - $this->x, my - $this->y))
                    break;
            }
        }
    };
    active_scene->frames.push_back(h);
    return h;
}
//----------------- /BEGIN FUNCTIONS ------------------
void UI::push_scene(handle s) {
    scene_stack.push(check_type<scene>(s));
    scene_stack.top()->render();
}

//##################[ MAIN LOOP ]#######################
void scene_proc(scene* sc, int message) {
    switch (message) {
        case KEY_RESIZE: {
            sc->render();
            break;
        }
        case KEY_MOUSE: {
            MEVENT me;
            getmouse(&me);
            if (me.bstate & BUTTON1_CLICKED) {
                for (frame* f : sc->frames) {
                    if (f->check_click(me.x, me.y))break;
                }
            }
            break;
        }
        default:
            break;
    }
}
void UI::run() {
    int message;
    scene_stack.top()->render();
    while ((message = getch()) != KEY_F(12)) {
        scene_proc(scene_stack.top(), message);
        if (scene_stack.empty())return;
    }
}
void UI::pop_scene() {
    if (scene_stack.empty())
        throw std::runtime_error("UI: scene stack is empty!");
    scene_stack.pop();
    if (!scene_stack.empty())
        scene_stack.top()->render();
}
handle UI::button(int x, int y, int width, const char* text, const std::function<void(handle)>& callback) {
    if (!active_scene)
        throw std::logic_error("UI: no active scene!");
    if (active_scene->frames.empty())
        throw std::logic_error("UI: no active frame in scene!");
    auto new_btn = new struct button();
    new_btn->x = x;
    new_btn->y = y;
    new_btn->width = width;
    new_btn->height = 2;
    new_btn->text = text;
    new_btn->on_click = [ callback ](handle h, int, int) {
        callback(h);
    };
    active_scene->frames.back()->elems.push_back(new_btn);
    return new_btn;
}
void UI::change_text(handle h, const char* text) {
    auto txt = check_type<textable>(h);
    txt->text = text;
    scene_stack.top()->render();
}
handle UI::add_check_box(int x, int y, int width, const char* text, bool checked,
                         const std::function<void(handle, bool)>& callback) {
    if (!active_scene)
        throw std::logic_error("UI: no active scene!");
    if (active_scene->frames.empty())
        throw std::logic_error("UI: no active frame in scene!");
    auto new_chb = new check_box();
    new_chb->x = x;
    new_chb->y = y;
    new_chb->width = width;
    new_chb->height = 3;
    new_chb->text = text;
    new_chb->checked = checked;
    new_chb->on_click = [ callback ](handle h, int, int) {
        auto chb = dynamic_cast<check_box*>(h);
        chb->checked = !chb->checked;
        scene_stack.top()->render();
        callback(chb, chb->checked);
    };
    active_scene->frames.back()->elems.push_back(new_chb);
    return new_chb;
}
handle UI::range_bar(int x, int y, int width, const char* text, float min, float max, float value) {
    if (!active_scene)
        throw std::logic_error("UI: no active scene!");
    if (active_scene->frames.empty())
        throw std::logic_error("UI: no active frame in scene!");
    auto rb = new struct range_bar();
    rb->x = x;
    rb->y = y;
    rb->text = text;
    rb->width = width;
    rb->height = 3;
    rb->min = min;
    rb->max = max;
    rb->value = value;
    active_scene->frames.back()->elems.push_back(rb);
    return rb;
}
void UI::change_value(handle h, float value) {
    auto v = check_type<valuable>(h);
    v->set_vaue(value);
    scene_stack.top()->render();
}
handle UI::track_bar(int x, int y, int width, const char* text, float min, float max, float value, float step,
                     const std::function<void(handle, float)>& callback) {
    if (!active_scene)
        throw std::logic_error("UI: no active scene!");
    if (active_scene->frames.empty())
        throw std::logic_error("UI: no active frame in scene!");
    auto tb = new struct track_bar();
    tb->x = x;
    tb->y = y;
    tb->text = text;
    tb->width = width;
    tb->height = 3;
    tb->min = min;
    tb->max = max;
    tb->step = step;
    tb->value = value;
    tb->on_click = [ callback ](handle h, int mx, int my) {
        auto trb = dynamic_cast<struct track_bar*>(h);
        if (mx == trb->x) // decrease by one step
            trb->set_vaue(trb->value - trb->step);
        else if (mx == (trb->x + trb->width - 1)) // increase by one step
            trb->set_vaue(trb->value + trb->step);
        else if (my > trb->y && my < (trb->y + trb->width - 1)) { // click in range
            float xx = mx - trb->x - 1;
            trb->set_vaue(xx / (trb->width - 2) * (trb->max - trb->min) + trb->min);
        }
        callback(trb, trb->value);
        scene_stack.top()->render();
    };
    active_scene->frames.back()->elems.push_back(tb);
    return tb;
}
handle UI::label(int x, int y, const char* text, color cl) {
    if (!active_scene)
        throw std::logic_error("UI: no active scene!");
    if (active_scene->frames.empty())
        throw std::logic_error("UI: no active frame in scene!");
    auto lbl = new struct label();
    lbl->x = x;
    lbl->y = y;
    lbl->text = text;
    lbl->color = (int) cl;
    active_scene->frames.back()->elems.push_back(lbl);
    return lbl;
}
#define MAX(x, y) ((x)>(y)?(x):(y))
void UI::message_box(const char* caption, const char* message) {
    auto modal = begin_scene(false, false);
    int w = int(MAX(strlen(message), strlen(caption))) + 2;
    int h = 6;
    int x = (getmaxx(stdscr) - w) / 2;
    int y = (getmaxy(stdscr) - h) / 2;
    begin_frame(x, y, w, h, caption);
    label(1, 2, message, color::Yellow);
    button(1, 3, w - 2, "OK", [ modal, this ](handle) {
        pop_scene();
        all_scenes.remove((scene*) modal);
        delete modal;
    });
    push_scene(modal);
}
}