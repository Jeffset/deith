//
// Created by marco on 30/01/17.
// C++ functional bindings for ncurses
//

#ifndef CURSES_TEST_CURSED_UI_H
#define CURSES_TEST_CURSED_UI_H

#include <string>
#include <memory>

namespace ncursed {
struct element;

using handle = element*;

enum class color {
    White = 10,
    Red,
    Green,
    Blue,
    Cyan,
    Yellow,
    Magenta,
};

class UI { // Singleton
    UI();
public:
    UI(const UI&) = delete;
    UI& operator=(const UI&)= delete;
    UI(UI&&) = default;
    UI& operator=(UI&&)= default;
    // singleton getter;
    static UI cursed_gui();
    ~UI();
    // factory methods
    handle begin_scene(bool border, bool cursor);
    handle begin_frame(int x, int y, int width, int height, const char* title);
    
    void push_scene(handle scene);
    void pop_scene();
    
    handle button(int x, int y, int width, const char* text, const std::function<void(handle)>& callback);
    handle add_check_box(int x, int y, int width, const char* text, bool checked,
                         const std::function<void(handle, bool)>& callback);
    handle range_bar(int x, int y, int width, const char* text,
                     float min = 0.f, float max = 100.f, float value = 0.f);
    handle track_bar(int x, int y, int width, const char* text, float min, float max, float value, float step,
                     const std::function<void(handle, float)>& callback);
    handle label(int x, int y, const char* text, color cl);
    
    void message_box(const char* caption, const char* message);
    
    void change_text(handle h, const char* text);
    void change_value(handle h, float new_val);
    //
    void run();
};
}

#endif //CURSES_TEST_CURSED_UI_H
