//
// Created by marco on 03/02/17.
//

#include "cursed_ui.h"

int main(void) {
    auto ui = ncursed::UI::cursed_gui();
    ncursed::handle scene1, scene2;
    scene1 = ui.begin_scene(true, false);
    ui.begin_frame(1, 8, 20, 12, "win-1");
    auto act = [ &ui ](ncursed::handle btn) {
        ui.change_text(btn, "clicked!");
        ui.pop_scene();
    };
    ui.button(1, 1, 8, "btn1(+)", act);
    ui.button(1, 4, 8, "btn2(*)", act);
    ui.add_check_box(1, 7, 10, "chb", true, [ &ui ](ncursed::handle h, bool ch) {
        ui.change_text(h, ch ? "ON" : "OFF");
    });
    
    scene2 = ui.begin_scene(true, false);
    const int WIDTH = 30;
    
    float progress = 0.f;
    ui.begin_frame(1, 1, WIDTH, 20, "win-2");
    ui.button(1, 1, WIDTH - 2, "display message", [ &ui ](ncursed::handle) {
        ui.message_box("message", "hello from cursed ui!");
    });
    ui.button(1, 4, WIDTH - 2, "goto the 1st scene", act);
    auto rb1 = ui.range_bar(1, 10, WIDTH - 2, "progress", 0.f, 100.f, 0.f);
    ui.button(1, 7, WIDTH - 2, "increase progress", [ &ui, &progress, rb1 ](ncursed::handle) {
        progress += 10.f;
        ui.change_value(rb1, progress);
    });
    auto lbl1 = ui.label(1, 17, "hello cursed world!", ncursed::color::Magenta);
    ui.track_bar(1, 14, WIDTH - 2, "adjust", 100.f, 200.f, 120.f, 5.f, [ &ui, lbl1 ](ncursed::handle, float val) {
        static char buffer[20];
        sprintf(buffer, "Current value = %.4f", val);
        ui.change_text(lbl1, buffer);
    });
    
    ui.push_scene(scene2);
    ui.push_scene(scene1);
    ui.push_scene(scene2);
    ui.push_scene(scene1);
    ui.push_scene(scene2);
    ui.run();
}