//
// Created by marco on 31/01/17.
//

#ifdef MYRIO
#include "ncurses/ncurses.h"
#else

#include "ncurses.h"

#endif

#include "elements.h"
#include <string.h>

WINDOW* ncursed::element::a_wnd = nullptr;

void ncursed::frame::render() {
    a_wnd = win;
    wclear(win);
    box(win, 0, 0);
    mvwaddstr(win, 0, 2, text.c_str());
    for (auto elem : elems)
        elem->render();
    wrefresh(win);
    a_wnd = stdscr;
}
ncursed::frame::frame(int x, int y, int width, int height) {
    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;
    win = newwin(height, width, y, x);
}
ncursed::frame::~frame() {
    for (auto e:elems)
        delete e;
    delwin(win);
}
void ncursed::scene::render() {
    curs_set(show_cursor);
    a_wnd = stdscr;
    clear();
    if (show_border)
        box(stdscr, 0, 0);
    refresh();
    for (auto f : frames)
        f->render();
    a_wnd = nullptr;
}
ncursed::scene::~scene() {
    for (frame* f : frames)
        delete f;
}
void ncursed::button::render() {
    int s = (int) text.size();
    wattron(a_wnd, COLOR_PAIR(BUTTON_BORDER));
    mvwhline(a_wnd, y, x, 0, width);
    mvwhline(a_wnd, y + height / 2, x, ACS_CKBOARD, width);
    mvwhline(a_wnd, y + height, x, 0, width);
    wattron(a_wnd, A_REVERSE);
    mvwaddstr(a_wnd, y + height / 2, x + (width - s) / 2, text.c_str());
    wattroff(a_wnd, COLOR_PAIR(BUTTON_BORDER) | A_REVERSE);
}
bool ncursed::clickable::check_click(int mx, int my) {
    if (mx < x || mx > x + width)return false;
    if (my < y || my > y + height)return false;
    on_click((element*) this, mx, my);
    return true;
}
void ncursed::check_box::render() {
    auto flag = checked ? COLOR_PAIR(CHECK_BOX_CHECKED) | A_BOLD : 0;
    mvwaddch(a_wnd, y, x, ACS_ULCORNER | 0);
    waddch(a_wnd, ACS_HLINE | 0);
    waddch(a_wnd, ACS_TTEE | 0);
    whline(a_wnd, 0, width - 4);
    mvwaddch(a_wnd, y, x + width - 1, ACS_URCORNER);
    //
    mvwaddch(a_wnd, y + 1, x, ACS_VLINE | 0);
    waddch(a_wnd, (checked ? ACS_CKBOARD | A_UNDERLINE : '-') | flag);
    waddch(a_wnd, ACS_VLINE | 0);
    if (checked)wattron(a_wnd, flag);
    waddstr(a_wnd, text.c_str());
    if (checked)wattroff(a_wnd, flag);
    whline(a_wnd, ' ', width - int(text.size()) - 3);
    mvwaddch(a_wnd, y + 1, x + width - 1, ACS_VLINE);
    //
    mvwaddch(a_wnd, y + 2, x, ACS_LLCORNER | 0);
    waddch(a_wnd, ACS_HLINE | 0);
    waddch(a_wnd, ACS_BTEE | 0);
    whline(a_wnd, 0, width - 4);
    mvwaddch(a_wnd, y + 2, x + width - 1, ACS_LRCORNER);
}
void ncursed::range_bar::render() {
    auto flag = COLOR_PAIR(CHECK_BOX_CHECKED) | A_BOLD | A_UNDERLINE;
    // frame
    mvwhline(a_wnd, y, x, 0, width);
    mvwhline(a_wnd, y + height - 1, x, 0, width);
    mvwvline(a_wnd, y, x, 0, height);
    mvwvline(a_wnd, y, x + width - 1, 0, height);
    mvwaddch(a_wnd, y, x, ACS_ULCORNER);
    mvwaddch(a_wnd, y, x + width - 1, ACS_URCORNER);
    mvwaddch(a_wnd, y + height - 1, x + width - 1, ACS_LRCORNER);
    mvwaddch(a_wnd, y + height - 1, x, ACS_LLCORNER);
    // title
    mvwaddstr(a_wnd, y, x + (width - int(text.size())) / 2, text.c_str());
    //
    auto vv = value > max ? max : value < min ? min : value;
    int w = int((vv - min) / (max - min) * (width - 2));
    for (int i = 0; i < height - 2; ++i) {
        mvwhline(a_wnd, y + 1 + i, x + 1, ACS_CKBOARD | flag, w);
    }
    static char buffer[20];
    int len = sprintf(buffer, "%.2f", value);
    wattron(a_wnd, flag);
    mvwaddstr(a_wnd, y + height - 1, x + (width - len) / 2, buffer);
    wattroff(a_wnd, flag);
}
void ncursed::track_bar::render() {
    range_bar::render();
    auto flag = COLOR_PAIR(CHECK_BOX_CHECKED) | A_BOLD;
    mvwaddch(a_wnd, y + height / 2, x, ACS_LARROW | flag);
    mvwaddch(a_wnd, y + height / 2, x + width - 1, ACS_RARROW | flag);
}
void ncursed::valuable::set_vaue(float val) {
    value = val > max ? max : val < min ? min : val;
}
void ncursed::label::render() {
    wmove(a_wnd, y, x);
    wattron(a_wnd, COLOR_PAIR(color));
    waddstr(a_wnd, text.c_str());
    wattroff(a_wnd, COLOR_PAIR(color));
}
