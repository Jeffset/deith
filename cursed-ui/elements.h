//
// Created by marco on 31/01/17.
//

#ifndef CURSES_TEST_ELEMENTS_H
#define CURSES_TEST_ELEMENTS_H

#include <list>
#include <string>
#include <functional>

namespace ncursed {

struct element {
    static WINDOW* a_wnd;
    virtual void render()=0;
    virtual ~element() {};
};

struct textable : public virtual element {
    std::string text;
};

struct colorable : public virtual element {
    int color;
};

struct sizable : public virtual element {
    int x, y, width, height;
};

struct clickable : public virtual sizable {
    std::function<void(element*, int, int)> on_click;
    bool check_click(int x, int y);
};

struct valuable : public virtual element {
    float min = 0.f, max = 100.f;
    float value = 0.f;
    void set_vaue(float val);
};

struct frame : public clickable, public textable {
    WINDOW* win;
    std::list<element*> elems;
    virtual void render() override;
    frame(int x, int y, int width, int height);
    ~frame();
};

struct scene : public element {
    bool show_border, show_cursor;
    std::list<frame*> frames;
    void render() final;
    ~scene();
};

#define BUTTON_BORDER 1
struct button : public clickable, public textable {
    virtual void render() override;
};
#define CHECK_BOX_CHECKED 2
struct check_box : public clickable, public textable {
    bool checked;
    virtual void render() override;
};

struct range_bar : public virtual sizable, public virtual textable, public virtual valuable {
    virtual void render() override;
};

struct track_bar : public range_bar, public virtual clickable {
    float step;
    virtual void render() override;
};

struct label : public textable, public sizable, public colorable {
    virtual void render() override;
};
    
}

#endif //CURSES_TEST_ELEMENTS_H
